<?
define("PRICECODE_MSK", 'Розничная МСК');
define("PRICEID_MSK", '2');

define("PRICECODE_MSK_ACTION", 'Акция Москва');
define("PRICEID_MSK_ACTION", '10');

define("PRICECODE_SPB", 'Розничная СПБ');
define("PRICEID_SPB", '6');

define("PRICECODE_SPB_ACTION", 'Акция СПб');
define("PRICEID_SPB_ACTION", '3');

define("PRICECODE_SPB_DEALER", 'Дилерская СПБ');
define("PRICEID_SPB_DEALER", '9');

define("PRICECODE_MSK_DEALER", 'Дилерская МСК');
define("PRICEID_MSK_DEALER", '5');

define("PRICECODE_SPB_DEALER_ACTION", 'Акция дилерская СПБ');
define("PRICEID_SPB_DEALER_ACTION", '15');

define("PRICECODE_MSK_DEALER_ACTION", 'Акция дилерская МСК');
define("PRICEID_MSK_DEALER_ACTION", '16');


define("IBLOCK_CATALOG", '42');
define("IBLOCK_NEWS", '6');
define("IBLOCK_ARTICLES", '14');
define("IBLOCK_GALLERY", '35');

define("KOLVO_SHT", 'KOLICHESTVO_SHT_M2');
//define("PRICECODE_ALL", serialize(array(PRICECODE_MSK, PRICECODE_MSK_ACTION, PRICECODE_SPB, PRICECODE_SPB_ACTION)));

define("REAL_PRICE_SPB", '1095');
define("REAL_PRICE_MSK", '1096');
define("REAL_PRICE_SPB_M2", '1098');
define("REAL_PRICE_MSK_M2", '1099');

/*Цены за м2. Символьные кода свойств*/
define("AMM2", 'AKTSIYA_MOSKVA_M2');
define("RMM2", 'ROZNICHNAYA_MSK_M2');
define("ASM2", 'AKTSIYA_SPB_M2');
define("RSM2", 'ROZNICHNAYA_SPB_M2');

define("RPS", 'REAL_PRICE_SPB');
define("RPM", 'REAL_PRICE_MSK');
define("RPSM2", 'REAL_PRICE_SPB_M2');
define("RPMM2", 'REAL_PRICE_MSK_M2');

define("DNDM", 'DO_NOT_DISPLAY_M');
define("DNDS", 'DO_NOT_DISPLAY_S');
define("IBLOCK_ACTIONS", '32');
define("IBLOCK_SKLAD_PROGRAMMA", '51');
define("IBLOCK_STATIC", '20');
define("IBLOCK_SETTINGS", '56');
define("CUSTOM_ITEMS", 'CUSTOM_ITEMS');

define("DEALERS_GROUP_ID", '5');

define("RUCENTER_GEOIP_INSTALLED", true);

define("KONKURS_SECTION_ID", 32915);

define("EMPTY_IMAGE_PATH", '/bitrix/templates/empty/images/empty_photo.png');

define("SECTION_HIT", 4);
define("SECTION_VIP", 5);
define("SECTION_NEW", 6);

define("SHOW_PRICE_SHT", 11);
define("SHOW_PRICE_M2", 12);

define("IBLOCK_BRANDS_MARKS", 75);

define("IBLOCK_KONKURS", '76');
define("IBLOCK_VOTES", '69');
define("KONKURS_VIEW_FIRST", 13);
define("KONKURS_VIEW_SECOND", 14);
define("KONKURS_VIEW_THIRD", 15);
define("KONKURS_DO_NOT_VOTE", 17);
define("KONKURS_DO_VOTE", 18);
define("KONKURS_CLOSED_SECTION_ID", 47860);
define("KONKURS_CLOSED_SECTION_CODE", 'proshedshie-konkursy');
define("KONKURS_CURRENT_SECTION_ID", 56717);
define("IBLOCK_SIMILAR", '77');
define("IBLOCK_LOST_BASKETS", '78');
define("IBLOCK_POPUPS", '79');
define("IBLOCK_MENU", '67');
define("IBLOCK_SAVE_FORMS", '80');
define("IBLOCK_FORM_MESSAGES", 52);
define("IBLOCK_PHOTO_BANNER", "81");

define("IBLOCK_TYPE_CONTACTS_REGIONS", 'service_information');

define("STORE_SPB_ID", 10);
define("STORE_MSK_ID", 11);

define("SALE_PROPVAL_ID", 77304);

define("SECTION_ID_TAKT_PLITKA", 56936);

define("BRANDS_TABLE_NAME", 'b_brend');

define("REGION_PROP_ACTIONS_SPB", 10359);
define("REGION_PROP_ACTIONS_MSK", 10360);

define("ORDER_PROP_ID_INN", 25);
define("ORDER_PROP_ID_KPP", 26);

define("PREFIX_ECOMM_PRODID_SPB_ID", 36);
define("PREFIX_ECOMM_PRODID_MSK_ID", 37);

define("PROP_CROSS_SELL", 3025);

define("ORDER_PROP_SLAVDOM_ID", 27);
define("ORDER_PROP_GOOGLE_CID", 29);
define("ORDER_PROP_YANDEX_CID", 28);