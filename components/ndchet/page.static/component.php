<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(CModule::IncludeModule("iblock")){
	$arResult=array();
	$arFlt=array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]);
	if($arParams["USE_CODE"]=="Y"){
		$arURI=explode("/", $_SERVER["REQUEST_URI"]);
		$cnt=count($arURI);
		$dsa=explode("?", $arURI[$cnt-1]);
		$arURI[$cnt-1]=$dsa[0];
		if($arURI[$cnt-1]=="index.php" || $arURI[$cnt-1]==""){
			$code=$arURI[$cnt-2];
		}else{
			$code=$arURI[$cnt-2]."/".$arURI[$cnt-1];
		}
		$arFlt["CODE"]=$code;
	}else{
		$arFlt["ID"]=$arParams["PAGE_ID"];
	}
	$res=CIblockElement::GetList(array(), $arFlt, false, false, array("ID", "NAME", "DETAIL_TEXT", "PREVIEW_PICTURE", "PREVIEW_TEXT", "PROPERTY_UF_SEO_FLAG", "PROPERTY_UF_META_TITLE", "PROPERTY_UF_META_K", "PROPERTY_UF_META_D", "PROPERTY_UF_SEO_TEXT"));
	if($itm=$res->GetNext()){
		$arResult["NAME"]=$itm["NAME"];
		$arResult["PREVIEW_PICTURE"] = CFile::GetPath($itm["PREVIEW_PICTURE"]);
		$arResult["PREVIEW_TEXT"] = $itm["PREVIEW_TEXT"];
		$arResult["TEXT"]=$itm["DETAIL_TEXT"];
		if($itm["PROPERTY_UF_SEO_FLAG_VALUE"]=="Да"){
			$arResult["SEO_TEXT"]=$itm["PROPERTY_UF_SEO_TEXT_VALUE"];
			$APPLICATION->SetPageProperty("title", $itm["PROPERTY_UF_META_TITLE_VALUE"]);
			$APPLICATION->SetPageProperty("description", $itm["PROPERTY_UF_META_D_VALUE"]);
			$APPLICATION->SetPageProperty("keywords", $itm["PROPERTY_UF_META_K_VALUE"]);
		}
		
	}else{
		$arResult["ERROR"][]="Page not found";
	}
	$this->IncludeComponentTemplate();
	if($arParams["ADD_SECTIONS_CHAIN"] && isset($arResult["PATH"]) && is_array($arResult["PATH"])){
		foreach($arResult["PATH"] as $arPath){
			$APPLICATION->AddChainItem($arPath["NAME"], $arPath["~SECTION_PAGE_URL"]);
		}
	}
}?>