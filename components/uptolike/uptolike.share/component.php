<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$module_id = "uptolike.share";
global $CACHE_MANAGER, $APPLICATION;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

if($this->StartResultCache(false, $arResult))
{
	$arResult["WIJET_CODE"] = COption::GetOptionString($module_id, "WIJET_CODE");
	
	$arResult["SHARE_LANGUAGE"] = COption::GetOptionString($module_id, "SHARE_LANGUAGE");
	
	$arResult["WIJET_CODE"] = CUptolikeShare::langIframe($arResult["WIJET_CODE"], $arResult["SHARE_LANGUAGE"]);
	
	$arResult["HORIZ_ALLIGMENT"] = COption::GetOptionString($module_id, "HORIZ_ALLIGMENT");
	
	$arResult["WIJET_CODE"] = CUptolikeShare::horizAlligmentIframe($arResult["WIJET_CODE"], $arResult["HORIZ_ALLIGMENT"]);
		
	$this->IncludeComponentTemplate();
}