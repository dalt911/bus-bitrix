<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arComponentDescription = array(
	"NAME"			=> GetMessage("UPTOLIKE_SHARE_COMPONENT_NAME"),
	"DESCRIPTION"	=> GetMessage("UPTOLIKE_SHARE_COMPONENT_DESCRIPTION"),
	"ICON" => "/images/icon.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "uptolike",
		"CHILD" => array(
			"ID" => "uptolike.share",
			"NAME" => GetMessage("UPTOLIKE_SHARE_GROUP_NAME"),
		),
	),
);
?>