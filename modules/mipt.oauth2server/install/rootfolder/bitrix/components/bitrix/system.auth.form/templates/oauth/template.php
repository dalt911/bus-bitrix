<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<tr class="list" align="center">
	<td colspan="2">
	<div class="access center">
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
	ShowMessage($arResult['ERROR_MESSAGE']);
?>
		<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=substr($arResult['AUTH_REGISTER_URL'], 0, strpos($arResult['AUTH_REGISTER_URL'], '?'))?>">
<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?endif?>
<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
<?endforeach?>
		<input type="hidden" name="AUTH_FORM" value="Y" />
		<input type="hidden" name="TYPE" value="AUTH" />
		<input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" size="25" placeholder="<?=GetMessage('AUTH_LOGIN')?>" autofocus required />
		<input type="password" name="USER_PASSWORD" maxlength="50" size="25" placeholder="<?=GetMessage('AUTH_PASSWORD')?>" required />
<?if ($arResult["CAPTCHA_CODE"]):?>
		<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br />
		<input type="text" name="captcha_word" maxlength="50" value="" placeholder="<?=GetMessage("AUTH_CAPTCHA_PROMT")?>" />
<?endif?>
		<input type="submit" class="btnaccess" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" /> 
		</form>
		</div>
	</td>
</tr>
<tr>
	<td colspan="2" class="bottombuttons" align="center">
	<noindex><a href="<?=$arResult['AUTH_FORGOT_PASSWORD']?>"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a></noindex>
	</td>
</tr>
