<?
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/php_interface/dbconn.php');
CModule::IncludeModuleEx('mipt.oauth2server');

$dsn = $DBType.':dbname='.$DBName.';host='.$DBHost;
$username = $DBLogin;
$password = $DBPassword;
require_once($_SERVER['DOCUMENT_ROOT'].'/oauth/OAuth2/Autoloader.php');
OAuth2\Autoloader::register();
$storage = new OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $username, 'password' => $password));
$server = new OAuth2\Server($storage, array(
	'always_issue_new_refresh_token' => true,
	'refresh_token_lifetime'         => 3600,
));
$server = CMIPToauth2server::Init($server, $storage);
?>