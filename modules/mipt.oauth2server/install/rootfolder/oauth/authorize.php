<?
require_once __DIR__.'/server.php';
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

if ($_REQUEST['do']=='logout' && $_REQUEST['backurl'] <> '') {
	$USER->Logout();
	LocalRedirect($_REQUEST['backurl']);
}
$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

if (!$server->validateAuthorizeRequest($request, $response)) {
    $response->send();
    die;
}
$sqlQ = $DB->Query('SELECT `value` FROM `oauth_settings` WHERE `name`=\'access_question_again\'');
$access_question_again = $sqlQ->Fetch();
$access_question_again = $access_question_again['value'];
$sqlQ = $DB->Query('SELECT `value` FROM `oauth_settings` WHERE `name`=\'view_site_name\'');
$view_site_name = $sqlQ->Fetch();
$view_site_name = $view_site_name['value'];
$sqlQ = $DB->Query('SELECT `value` FROM `oauth_settings` WHERE `name`=\'auth_url\'');
$auth_url = $sqlQ->Fetch();
$auth_url = $auth_url['value'];

$lang = null;
if (isset($_REQUEST['lang'])) {
	$lang = htmlspecialcharsbx($_REQUEST['lang']);
}
else {
	if ($USER->IsAuthorized()) {
		$rsSite = CSite::GetByID($USER->GetParam('LID'));
		$arSite = $rsSite->Fetch();
		$lang = $arSite['LANGUAGE_ID'];
	}
	else {
		$rsSites = CSite::GetList();
		while ($arSite = $rsSites->Fetch()) {
			if ($arSite['DEF'] == 'Y') {
				$defSite = $arSite;
			}
		}
		$lang = $defSite['LANGUAGE_ID'];
	}
}

require_once(dirname(__FILE__).'/lang/'.$lang.'/authorize.php');

if (empty($_POST['authorized'])) {
  if ($USER->IsAuthorized()) {
	if ($storage->getAuthorizedClientByUser($_GET['client_id'], $USER->GetID()) && $access_question_again == 'no'){
		echo ('<form method="post" id="frm">
			<input type="hidden" name="authorized" value="yes">
			<noscript>
			<button type="submit">'.GetMessage('OAUTH2_SERVER_CONTINUE').'</button>
			</noscript>
			</form>
			<script language="JavaScript">
			document.getElementById("frm").submit();
			</script>');
	}
	elseif (!$storage->getAuthorizedClientByUser($_GET['client_id'], $USER->GetID()) || $access_question_again == 'yes') {
        $arUser = CUser::GetByID($USER->GetID())->Fetch();
        ?>
        <!DOCTYPE HTML>
		<html>
		<head>
		<meta charset="<?=(BX_UTF == true ? 'utf-8' : 'windows-1251')?>">
		<title>OAuth2</title>
		<link href="style.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<table cellspacing="0" cellpadding="0">
			<tr class="head">
		    	<td class="logo">
		        	<a class="white" href="<?=(($_SERVER['HTTPS'] == 'off') || ($_SERVER['HTTPS'] == '') ? 'http://' : 'https://').$_SERVER['HTTP_HOST']?>"><?=$view_site_name?></a>
		    	</td>
		        <td class="userauth">
		        <form action="authorize.php?do=logout" method="POST">
		            <?=CFile::ShowImage($arUser["PERSONAL_PHOTO"],   32, 32, "border=0", "", true)?>
		        	<?=$USER->GetFullName()?>
		        	<button class="white logoutbutton" type="submit"><?=GetMessage('OAUTH2_SERVER_LOGOUT')?></button>
		        	<input type="hidden" name="backurl" value="<?=$_SERVER['REQUEST_URI']?>">
		        </form>
		        </td>
		    </tr>
		    <tr class="welcome">
		    	<td colspan=2>
		        <?=GetMessage('OAUTH2_SERVER_HELLO')?> <b><?=$USER->GetFirstName()?>!</b>
		        <br>
		        <?=GetMessage('OAUTH2_SERVER_APP')?> <span class="appname"><?=htmlspecialcharsbx($_GET['client_id'])?></span> <?=GetMessage('OAUTH2_SERVER_APP_REQUEST')?>
		        </td>
		    </tr>
		    <tr class="list">
		    	<td colspan="2">
		        	<div class="access">
		        		<?
		        		if (in_array('userinfo', explode(' ', htmlspecialcharsbx($_REQUEST['scope'])))) {?>
		           	    <div class="userinfo">
		                	<span class="accname"><?=GetMessage('OAUTH2_SERVER_USERINFO')?></span><br>
		                    <span class="accdesc"><?=GetMessage('OAUTH2_SERVER_USERINFO_DESC')?></span>
		                </div>
		                <?}
		                if (in_array('email', explode(' ', htmlspecialcharsbx($_REQUEST['scope'])))) {?>
		                <div class="email">
		                	<span class="accname"><?=GetMessage('OAUTH2_SERVER_EMAIL')?></span><br>
		                    <span class="accdesc"><?=substr_replace($USER->GetEmail(), '***', 2, strpos($USER->GetEmail(), '@') - 2)?></span>
		                </div>
		                <?}?>
		            </div>
		        </td>
		    </tr>
		    <tr>
		    	<td colspan="2" class="bottombuttons" align="right">
		        	<form action="" method="POST">
		            	<button class="btnaccess" name="authorized" value="yes"><?=GetMessage('OAUTH2_SERVER_ACCEPT')?></button><button class="btncancel" name="authorized" value="no"><?=GetMessage('OAUTH2_SERVER_CANCEL')?></button>
		            </form>
		        </td>
		    </tr>
		</table>
		</body>
		</html>
		<?
	}
  }
  else {
  	?>
        <!DOCTYPE HTML>
		<html>
		<head>
		<meta charset="<?=(BX_UTF == true ? 'utf-8' : 'windows-1251')?>">
		<title>OAuth2</title>
		<link href="style.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<table cellspacing="0" cellpadding="0">
			<tr class="head">
		    	<td class="logo">
		        	<a class="white" href="<?=(($_SERVER['HTTPS'] == 'off') || ($_SERVER['HTTPS'] == '') ? 'http://' : 'https://').$_SERVER['HTTP_HOST']?>"><?=$view_site_name?></a>
		    	</td>
		        <td class="userauth">
		        	<a class="white" href="<?=$auth_url.'?register=yes'?>"><?=GetMessage('OAUTH2_SERVER_REGISTER')?></a>
		        </td>
		    </tr>
		    <tr class="welcome">
		    	<td colspan=2>
		    		<?=GetMessage('OAUTH2_SERVER_FOR_CONTINUE')?> <b><?=$_SERVER['HTTP_HOST']?></b>
		        </td>
		    </tr>
		        	<?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "oauth", Array(
						 "REGISTER_URL" => $auth_url,
					     "FORGOT_PASSWORD_URL" => $auth_url,
					     "PROFILE_URL" => "",
					     "SHOW_ERRORS" => "N" 
						 ));?>
		</table>
		</body>
		</html>

    <?
  }
  

}
else {
$is_authorized = ($_POST['authorized'] === 'yes');
$userid = $USER->GetID();
$server->handleAuthorizeRequest($request, $response, $is_authorized, $userid);
$response->send();
}
?>