<?
$MESS['OAUTH2_SERVER_CONTINUE'] = 'Continue';
$MESS['OAUTH2_SERVER_LOGOUT'] = 'Logout';
$MESS['OAUTH2_SERVER_HELLO'] = 'Hello,';
$MESS['OAUTH2_SERVER_APP'] = 'Application';
$MESS['OAUTH2_SERVER_APP_REQUEST'] = 'requests access to your account';
$MESS['OAUTH2_SERVER_USERINFO'] = 'User info';
$MESS['OAUTH2_SERVER_USERINFO_DESC'] = 'Application will be available to your personal data';
$MESS['OAUTH2_SERVER_EMAIL'] = 'E-Mail';
$MESS['OAUTH2_SERVER_ACCEPT'] = 'Accept';
$MESS['OAUTH2_SERVER_CANCEL'] = 'Decline';
$MESS['OAUTH2_SERVER_REGISTER'] = 'Register';
$MESS['OAUTH2_SERVER_FOR_CONTINUE'] = 'To continue, you need to log in';
?>