<?
$MESS['OAUTH2_SERVER_CONTINUE']      = 'Продолжить';
$MESS['OAUTH2_SERVER_LOGOUT']        = 'Выйти';
$MESS['OAUTH2_SERVER_HELLO']         = 'Здравствуйте,';
$MESS['OAUTH2_SERVER_APP']           = 'Приложение';
$MESS['OAUTH2_SERVER_APP_REQUEST']   = 'запрашивает доступ к Вашему аккаунту';
$MESS['OAUTH2_SERVER_USERINFO']      = 'Информация о пользователе';
$MESS['OAUTH2_SERVER_USERINFO_DESC'] = 'Приложению будут доступны Ваши личные данные';
$MESS['OAUTH2_SERVER_EMAIL']         = 'E-Mail пользователя';
$MESS['OAUTH2_SERVER_ACCEPT']        = 'Разрешить';
$MESS['OAUTH2_SERVER_CANCEL']        = 'Отмена';
$MESS['OAUTH2_SERVER_REGISTER']      = 'Регистрация';
$MESS['OAUTH2_SERVER_FOR_CONTINUE']  = 'Для продолжения Вам необходимо войти в';
?>