<?
require_once __DIR__.'/server.php';
require_once __DIR__.'/api.class.php';

$exts_dir = __DIR__.'/exts';
$ext_filename_end = '.apiext.php';
if (file_exists($exts_dir)) {
	$files = scandir($exts_dir);
	foreach ($files as $filename) {
		if (substr($filename, -1*strlen($ext_filename_end)) == $ext_filename_end) {
			require_once __DIR__.'/exts/'.$filename;
		}
	}

}

// Handle a request for an OAuth2.0 Access Token and send the response to the client
if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
    $server->getResponse()->send();
    die;
}

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());
$scopes = explode(' ', $token['scope']);
$user_id = $token['user_id'];

$requestget = htmlspecialchars($_GET['get']);
//API
$evalstr = 'switch ($requestget) {
';
foreach ($MIPTOAuth2ServerExtends as $APIClass => $Methods) {
	eval('$API[\''.$APIClass.'\'] = new '.$APIClass.';');
	foreach ($Methods as $func => $case) {
		$evalstr .= '	case \''.$case.'\':
';
		$evalstr .= '		$result = $API[\''.$APIClass.'\']->'.$func.'; break;
';
	}
}
$evalstr .='}';
eval($evalstr);

echo json_encode($result, true);
?>