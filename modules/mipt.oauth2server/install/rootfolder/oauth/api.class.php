<?
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

$MIPTOAuth2ServerExtends['ServerAPI'] = array (
	'GetUserBasicInfo()' => 'userinfo',
	'GetUserEmail()' => 'email',
	);

class ServerAPI {

	protected $BXUser;

	protected function CanDoThis($scope) {
		global $scopes;
		return in_array($scope, $scopes);
	}
	
	protected function GetUserParamsByID($s_ParamKey = false, $i_UserID = false)
	{
		if(! $i_UserID)
			return; 	
		
		$ar_User = CUser::GetByID(intval($i_UserID))->Fetch();
		return (!$s_ParamKey) ? $ar_User : $ar_User[$s_ParamKey]; 
	}
	
	function __construct() {
		global $user_id;
		$this->BXUser = self::GetUserParamsByID('', $user_id);
	}
	
	function GetUserBasicInfo() {
		//Insert in the $need_scope the string what scope need to DO THIS method
		$need_scope = 'userinfo';
		$result = array (
			'id' => $this->BXUser['ID'],
			'firstname' => $this->BXUser['NAME'],
			'lastname' => $this->BXUser['LAST_NAME'],
			'secondname' => $this->BXUser['SECOND_NAME'],
			'login' => $this->BXUser['LOGIN']
		);
		return ($this->CanDoThis($need_scope) ? $result : '');
	}
	
	function GetUserEmail() {
		//Insert in the $need_scope the string what scope need to DO THIS method
		$need_scope = 'email';
		$result = array (
			'email' => $this->BXUser['EMAIL'],
		);
		return ($this->CanDoThis($need_scope) ? $result : '');
	}
	
}
?>