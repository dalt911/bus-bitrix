<?
$MESS['OAUTH2_SERVER_SETTINGS_TITLE']                      = 'OAuth2.Server: General settings';
$MESS['OAUTH2_SERVER_SETTINGS_SAVE_OK']                    = 'Settings saved successfully!';
$MESS['OAUTH2_SERVER_SETTINGS_SAVE_CANT']                  = 'Can\'t save changes';
$MESS['OAUTH2_SERVER_SETTINGS_HEADER_NAME_TITLE']          = 'Parameter';
$MESS['OAUTH2_SERVER_SETTINGS_HEADER_VALUE_TITLE']         = 'Value';
$MESS['OAUTH2_SERVER_SETTINGS_SAVE_QUESTION']              = 'Do you really want to save settings?';
$MESS['OAUTH2_SERVER_SETTINGS_SAVE']                       = 'Save';
$MESS['OAUTH2_SERVER_SETTINGS_ENABLE']                     = 'Enable';
$MESS['OAUTH2_SERVER_SETTINGS_ACCESS_QUESTION_AGAIN_NAME'] = 'Request each time';
$MESS['OAUTH2_SERVER_SETTINGS_ACCESS_QUESTION_AGAIN_DESC'] = 'Each time ask the user for permission to provide access client/application';
$MESS['OAUTH2_SERVER_SETTINGS_COLOR_SET_NAME']             = 'The color palette';
$MESS['OAUTH2_SERVER_SETTINGS_COLOR_SET_DESC']             = 'The main color of page providing access';
$MESS['OAUTH2_SERVER_SETTINGS_VIEW_SITE_NAME_NAME']        = 'Title';
$MESS['OAUTH2_SERVER_SETTINGS_VIEW_SITE_NAME_DESC']        = 'The title displayed on page providing access (HTML supported)';
$MESS['OAUTH2_SERVER_SETTINGS_AUTH_URL_NAME']              = 'Link to auth page';
$MESS['OAUTH2_SERVER_SETTINGS_AUTH_URL_DESC']              = 'Link to the login, registration, password recovery page';
?>