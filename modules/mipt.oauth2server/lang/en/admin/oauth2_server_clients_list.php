<?
$MESS['OAUTH2_SERVER_APPLIST_TITLE']             = 'OAuth2.Server: Clients/Apps list';
$MESS['OAUTH2_SERVER_APPLIST_HEADER_INFO_TITLE'] = 'Client/App info';
$MESS['OAUTH2_SERVER_APPLIST_HEADER_DO_TITLE']   = 'Do';
$MESS['OAUTH2_SERVER_APP_REDIRECT_URI']          = 'Redirect URI';
$MESS['OAUTH2_SERVER_APP_SCOPES']                = 'Scopes';
$MESS['OAUTH2_SERVER_APP_EDIT']                  = 'Edit';
$MESS['OAUTH2_SERVER_APP_EDIT_CANT']             = 'Can\'t edit client/app #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APP_ADD_CANT']              = 'Can\'t add client/app #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APP_ADD_CANT_EXIST']        = 'Client/App #CLIENT_ID# is already exists';
$MESS['OAUTH2_SERVER_APP_ADD_OK']                = 'Client/App #CLIENT_ID# added successfully!';
$MESS['OAUTH2_SERVER_APP_DELETE']                = 'Delete';
$MESS['OAUTH2_SERVER_APP_DELETE_QUESTION']       = 'Do you really want to delete #CLIENT_ID# from clients/apps list?';
$MESS['OAUTH2_SERVER_APP_DELETE_OK']             = 'Client/App #CLIENT_ID# deleted successfully!';
$MESS['OAUTH2_SERVER_APP_DELETE_CANT']           = 'Can\'t delete client/app #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APP_ADD']                   = 'Add';
?>