<?
$MESS['OAUTH2_MENU_TITLE']                 = 'OAuth2 settings';
$MESS['OAUTH2_MENU_SERVER_CLIENTS']        = 'Server: Clients/Apps';
$MESS['OAUTH2_MENU_SERVER_CLIENTS_TITLE']  = 'OAuth2.Server: Clients/Apps list';
$MESS['OAUTH2_MENU_SERVER_SCOPES']         = 'Server: Scopes';
$MESS['OAUTH2_MENU_SERVER_SCOPES_TITLE']   = 'OAuth2.Server: List of API scopes';
$MESS['OAUTH2_MENU_SERVER_SETTINGS']       = 'Server: General settings';
$MESS['OAUTH2_MENU_SERVER_SETTINGS_TITLE'] = 'OAuth2.Server: General settings';
?>