<?
$MESS['OAUTH2_SERVER_APPADDEDIT_ADD_TITLE']          = 'OAuth2.Server: Add new client/app';
$MESS['OAUTH2_SERVER_APPADDEDIT_EDIT_TITLE']         = 'OAuth2.Server: Edit client/app: ';
$MESS['OAUTH2_SERVER_APPINFO_HEADER_PARAM_TITLE']    = 'Parameter';
$MESS['OAUTH2_SERVER_APPINFO_HEADER_VALUE_TITLE']    = 'Value';
$MESS['OAUTH2_SERVER_APPINFO_SECRET_DESC']           = '64 symbols a-z, A-Z, 0-9';
$MESS['OAUTH2_SERVER_APPADDEDIT_GENSECRET']          = 'Generate new';
$MESS['OAUTH2_SERVER_APPADDEDIT_GENSECRET_QUESTION'] = 'Do you really want to generate a new Client Secret for this client/app?';
$MESS['OAUTH2_SERVER_APPINFO_REDIRECT_URI']          = 'Redirect URI to page for receive code';
$MESS['OAUTH2_SERVER_APPINFO_FIELD_MAY_BE_EMPTY']    = 'This field may be empty, but this is not safety';
$MESS['OAUTH2_SERVER_APPINFO_SCOPES']                = 'Scopes';
$MESS['OAUTH2_SERVER_APPINFO_GRANT_TYPE']            = 'Grant type';
$MESS['OAUTH2_SERVER_APPINFO_EDIT_QUESTION']         = 'Do you really want to save changes?';
$MESS['OAUTH2_SERVER_APPINFO_SAVE']                  = 'Save';
$MESS['OAUTH2_SERVER_APP_ADD']                       = 'Add';
$MESS['OAUTH2_SERVER_APP_ADD_CANT_EXIST']            = 'Client/App #CLIENT_ID# is already exists';
$MESS['OAUTH2_SERVER_APPINFO_EMPTY_FIELDS']          = 'One or more fields are empty or filled incorrectly';
$MESS['OAUTH2_SERVER_APPINFO_CANT_UPDATE_NOT_EXIST'] = 'Can\'t save changes. Client/App #CLIENT_ID# not exists';
$MESS['OAUTH2_SERVER_APPINFO_CANT_UPDATE']           = 'Can\'t save changes for client/app #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APPINFO_UPDATE_OK']             = 'Changes saved successfully!';
?>