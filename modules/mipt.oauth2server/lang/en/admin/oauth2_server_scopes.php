<?
$MESS['OAUTH2_SERVER_SCOPES_TITLE']                  = 'OAuth2.Server: List of API scopes';
$MESS['OAUTH2_SERVER_SCOPES_SAVE_OK']                = 'Changes saved successfully!';
$MESS['OAUTH2_SERVER_SCOPES_SAVE_CANT']              = 'Can\'t save changes';
$MESS['OAUTH2_SERVER_SCOPES_HEADER_NAME_TITLE']      = 'Name';
$MESS['OAUTH2_SERVER_SCOPES_HEADER_ISDEFAULT_TITLE'] = 'Is default';
$MESS['OAUTH2_SERVER_SCOPES_HEADER_DESC_TITLE']      = 'Description';
$MESS['OAUTH2_SERVER_SCOPES_SAVE_QUESTION']          = 'Do you really want to save changes?';
$MESS['OAUTH2_SERVER_SCOPES_SAVE']                   = 'Save';
$MESS['OAUTH2_SERVER_SCOPES_DESC_USERINFO']          = 'Basic user\'s info (ID, First name, Second name, Third name, Login)';
$MESS['OAUTH2_SERVER_SCOPES_DESC_EMAIL']             = 'E-Mail';
?>