<?
$MESS['OAUTH2_SERVER_APPADDEDIT_ADD_TITLE']          = 'OAuth2.Сервер: Добавить новый клиент/приложение (App)';
$MESS['OAUTH2_SERVER_APPADDEDIT_EDIT_TITLE']         = 'OAuth2.Сервер: Редактировать клиент/приложение (App): ';
$MESS['OAUTH2_SERVER_APPINFO_HEADER_PARAM_TITLE']    = 'Параметр';
$MESS['OAUTH2_SERVER_APPINFO_HEADER_VALUE_TITLE']    = 'Значение';
$MESS['OAUTH2_SERVER_APPINFO_SECRET_DESC']           = '64 символа a-z, A-Z, 0-9';
$MESS['OAUTH2_SERVER_APPADDEDIT_GENSECRET']          = 'Генерировать новый';
$MESS['OAUTH2_SERVER_APPADDEDIT_GENSECRET_QUESTION'] = 'Вы уверены, что хотите сгенерировать новый Client Secret?';
$MESS['OAUTH2_SERVER_APPINFO_REDIRECT_URI']          = 'URI для редиректа на страницу получения кода';
$MESS['OAUTH2_SERVER_APPINFO_FIELD_MAY_BE_EMPTY']    = 'Это поле может быть пустым, но это не безопасно';
$MESS['OAUTH2_SERVER_APPINFO_SCOPES']                = 'Разрешения';
$MESS['OAUTH2_SERVER_APPINFO_GRANT_TYPE']            = 'Тип подключения';
$MESS['OAUTH2_SERVER_APPINFO_EDIT_QUESTION']         = 'Вы уверены, что хотите сохранить изменения?';
$MESS['OAUTH2_SERVER_APPINFO_SAVE']                  = 'Сохранить';
$MESS['OAUTH2_SERVER_APP_ADD']                       = 'Добавить';
$MESS['OAUTH2_SERVER_APP_ADD_CANT_EXIST']            = 'Клиент #CLIENT_ID# уже существует: ';
$MESS['OAUTH2_SERVER_APPINFO_EMPTY_FIELDS']          = 'Одно или несколько обязательных полей пусты или неправильно заполнены';
$MESS['OAUTH2_SERVER_APPINFO_CANT_UPDATE_NOT_EXIST'] = 'Невозможно сохранить изменения для несуществуещего клиента #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APPINFO_CANT_UPDATE']           = 'Невозможно сохранить изменения для клиента #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APPINFO_UPDATE_OK']             = 'Изменения успешно сохранены!';
?>