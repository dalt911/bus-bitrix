<?
$MESS['OAUTH2_SERVER_SCOPES_TITLE']                  = 'OAuth2.Сервер: Список разрешений к API (Scopes)';
$MESS['OAUTH2_SERVER_SCOPES_SAVE_OK']                = 'Изменения успешно сохранены!';
$MESS['OAUTH2_SERVER_SCOPES_SAVE_CANT']              = 'Невозможно сохранить изменения';
$MESS['OAUTH2_SERVER_SCOPES_HEADER_NAME_TITLE']      = 'Название';
$MESS['OAUTH2_SERVER_SCOPES_HEADER_ISDEFAULT_TITLE'] = 'Выдавать по умолчанию';
$MESS['OAUTH2_SERVER_SCOPES_HEADER_DESC_TITLE']      = 'Описание';
$MESS['OAUTH2_SERVER_SCOPES_SAVE_QUESTION']          = 'Вы уверены, что хотите сохранить изменения?';
$MESS['OAUTH2_SERVER_SCOPES_SAVE']                   = 'Сохранить';
$MESS['OAUTH2_SERVER_SCOPES_DESC_USERINFO']          = 'Базовые данные о пользователе (ID, Имя, Фамилия, Отчество, Логин)';
$MESS['OAUTH2_SERVER_SCOPES_DESC_EMAIL']             = 'E-Mail пользователя';
?>