<?
$MESS['OAUTH2_SERVER_SETTINGS_TITLE']                      = 'OAuth2.Сервер: Общие настройки';
$MESS['OAUTH2_SERVER_SETTINGS_SAVE_OK']                    = 'Настройки успешно сохранены!';
$MESS['OAUTH2_SERVER_SETTINGS_SAVE_CANT']                  = 'Невозможно сохранить настройки';
$MESS['OAUTH2_SERVER_SETTINGS_HEADER_NAME_TITLE']          = 'Параметр';
$MESS['OAUTH2_SERVER_SETTINGS_HEADER_VALUE_TITLE']         = 'Значение';
$MESS['OAUTH2_SERVER_SETTINGS_SAVE_QUESTION']              = 'Вы уверены, что хотите сохранить настройки?';
$MESS['OAUTH2_SERVER_SETTINGS_SAVE']                       = 'Сохранить';
$MESS['OAUTH2_SERVER_SETTINGS_ENABLE']                     = 'Включить';
$MESS['OAUTH2_SERVER_SETTINGS_ACCESS_QUESTION_AGAIN_NAME'] = 'Запрос каждый раз';
$MESS['OAUTH2_SERVER_SETTINGS_ACCESS_QUESTION_AGAIN_DESC'] = 'Каждый раз запрашивать у пользователя разрешение на предоставление доступа приложению';
$MESS['OAUTH2_SERVER_SETTINGS_COLOR_SET_NAME']             = 'Цветовая палитра';
$MESS['OAUTH2_SERVER_SETTINGS_COLOR_SET_DESC']             = 'Основной цвет палитры оформления страницы разрешения доступа и авторизации';
$MESS['OAUTH2_SERVER_SETTINGS_VIEW_SITE_NAME_NAME']        = 'Заголовок';
$MESS['OAUTH2_SERVER_SETTINGS_VIEW_SITE_NAME_DESC']        = 'Заголовок, отображаемый на странице запроса разрешения (поддерживается HTML)';
$MESS['OAUTH2_SERVER_SETTINGS_AUTH_URL_NAME']              = 'Ссылка на страницу авторизации';
$MESS['OAUTH2_SERVER_SETTINGS_AUTH_URL_DESC']              = 'Ссылка на страницу авторизации, регистрации, восстановления пароля';
?>