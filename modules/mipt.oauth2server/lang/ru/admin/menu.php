<?
$MESS['OAUTH2_MENU_TITLE']                 = 'Настройки OAuth2';
$MESS['OAUTH2_MENU_SERVER_CLIENTS']        = 'Сервер: Клиенты/приложения (Apps)';
$MESS['OAUTH2_MENU_SERVER_CLIENTS_TITLE']  = 'OAuth2.Сервер: Список разрешенных клиентов/приложений (Apps)';
$MESS['OAUTH2_MENU_SERVER_SCOPES']         = 'Сервер: Разрешения (Scopes)';
$MESS['OAUTH2_MENU_SERVER_SCOPES_TITLE']   = 'OAuth2.Сервер: Список разрешений к API (Scopes)';
$MESS['OAUTH2_MENU_SERVER_SETTINGS']       = 'Сервер: Общие настройки';
$MESS['OAUTH2_MENU_SERVER_SETTINGS_TITLE'] = 'OAuth2.Сервер: Общие настройки';
?>