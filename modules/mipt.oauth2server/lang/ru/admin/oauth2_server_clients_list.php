<?
$MESS['OAUTH2_SERVER_APPLIST_TITLE']             = 'OAuth2.Сервер: Список разрешенных клиентов/приложений (Apps)';
$MESS['OAUTH2_SERVER_APPLIST_HEADER_INFO_TITLE'] = 'Информация о приложении';
$MESS['OAUTH2_SERVER_APPLIST_HEADER_DO_TITLE']   = 'Действия';
$MESS['OAUTH2_SERVER_APP_REDIRECT_URI']          = 'URI для редиректа';
$MESS['OAUTH2_SERVER_APP_SCOPES']                = 'Разрешения';
$MESS['OAUTH2_SERVER_APP_EDIT']                  = 'Редактировать';
$MESS['OAUTH2_SERVER_APP_EDIT_CANT']             = 'Невозможно отредактировать клиент #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APP_ADD_CANT']              = 'Невозможно добавить клиент #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APP_ADD_CANT_EXIST']        = 'Такой клиент #CLIENT_ID# уже существует';
$MESS['OAUTH2_SERVER_APP_ADD_OK']                = 'Клиент #CLIENT_ID# успешно добавлен!';
$MESS['OAUTH2_SERVER_APP_DELETE']                = 'Удалить';
$MESS['OAUTH2_SERVER_APP_DELETE_QUESTION']       = 'Вы уверены, что хотите удалить #CLIENT_ID# из списка разрешенных клиентов?';
$MESS['OAUTH2_SERVER_APP_DELETE_OK']             = 'Клиент #CLIENT_ID# успешно удален!';
$MESS['OAUTH2_SERVER_APP_DELETE_CANT']           = 'Невозможно удалить клиент #CLIENT_ID#';
$MESS['OAUTH2_SERVER_APP_ADD']                   = 'Добавить';
?>