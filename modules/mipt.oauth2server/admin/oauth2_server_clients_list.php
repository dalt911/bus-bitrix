<?
error_reporting(E_ALL);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mipt.oauth2server/include.php");
IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mipt.oauth2server");
if ($POST_RIGHT == "D"){
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));}
?>

<?
$APPLICATION->SetTitle(GetMessage('OAUTH2_SERVER_APPLIST_TITLE'));
global $DB;

if ($REQUEST_METHOD == 'POST' && $POST_RIGHT == 'W' && $_REQUEST['action'] == 'delete' && $_REQUEST['client_id'] <> '' && check_bitrix_sessid()) {
	$q = $DB->Query("DELETE FROM `oauth_clients` WHERE client_id = '".$DB->ForSql($_REQUEST['client_id'])."'");
	if (intval($q->AffectedRowsCount())>0){
		LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANGUAGE_ID."&mess=deleteok&client_id=".htmlspecialcharsbx($_REQUEST['client_id']));
	}
	else {
		LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANGUAGE_ID."&mess=delcant&client_id=".htmlspecialcharsbx($_REQUEST['client_id']));
	}
}

$sqlRes = $DB->Query('SELECT client_id, client_secret, redirect_uri, scope FROM `oauth_clients`');

$appList = array();
while ($row = $sqlRes->Fetch()) {
	array_push($appList, $row);
}
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
if ($_REQUEST['mess'] <> '' && $_REQUEST['client_id'] <> '') {
	$messtypes = array(
		"deleteok"     =>    "OK".GetMessage('OAUTH2_SERVER_APP_DELETE_OK', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
		"delcant"      => "ERROR".GetMessage('OAUTH2_SERVER_APP_DELETE_CANT', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
		"addok"        =>    "OK".GetMessage('OAUTH2_SERVER_APP_ADD_OK', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
		"addcant"      => "ERROR".GetMessage('OAUTH2_SERVER_APP_ADD_CANT', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
		"addcantexist" => "ERROR".GetMessage('OAUTH2_SERVER_APP_ADD_CANT_EXIST', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
		"editcant"     => "ERROR".GetMessage('OAUTH2_SERVER_APP_EDIT_CANT', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
	);
	if (array_key_exists($_REQUEST['mess'], $messtypes)) {
		if (substr($messtypes[$_REQUEST['mess']], 0, 2) == "OK") {
			$TYPE = "OK";
			$messtypes[$_REQUEST['mess']] = substr($messtypes[$_REQUEST['mess']], 2);
		} elseif (substr($messtypes[$_REQUEST['mess']], 0, 5) == "ERROR") {
			$TYPE = "ERROR";
			$messtypes[$_REQUEST['mess']] = substr($messtypes[$_REQUEST['mess']], 5);
		}
		CAdminMessage::ShowMessage(array("MESSAGE"=>$messtypes[$_REQUEST['mess']], "TYPE"=>$TYPE));
	}
}
?>
<?
/*$aTabs = array(
	array('DIV'=>'tab1', 'TAB'=>GetMessage('OAUTH2_SERVER_TAB_CLIENTS'), 'TITLE'=>GetMessage('OAUTH2_SERVER_TAB_CLIENTS_TITLE')),
	array('DIV'=>'tab2', 'TAB'=>GetMessage('OAUTH2_SERVER_TAB_SCOPES'), 'TITLE'=>GetMessage('OAUTH2_SERVER_TAB_SCOPES_TITLE')),
);
$tabControl = new CAdminTabControl('tabControl', $aTabs);
$tabControl->Begin();
?>

<?
$tabControl->BeginNextTab();*/
?>
<table class="adm-list-table">
	<tr class="adm-list-table-header">
		<td class="adm-list-table-cell" colspan=2>
			<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_APPLIST_HEADER_INFO_TITLE');?></div>
		</td>
		<td class="adm-list-table-cell">
			<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_APPLIST_HEADER_DO_TITLE');?></div>
		</td>
	</tr>
	<?
	$i=0;
	foreach ($appList as $key => $row) {
		$i++;
	?>
	<tr class="adm-list-table-row">
		<td class="adm-list-table-cell">
			Client ID
		</td>
		<td class="adm-list-table-cell">
			<?=$row['client_id']?>
		</td>
		<td class="adm-list-table-cell" rowspan=4>
			<p>
				<form action="/bitrix/admin/oauth2_server_clients_add_edit.php" method="GET">
					<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">
					<input type="hidden" name="do" value="edit">
					<input type="hidden" name="client_id" value="<?=$row['client_id']?>">
					<input type="submit" class="adm-btn-green" value="<?=GetMessage('OAUTH2_SERVER_APP_EDIT')?>" />
				</form>
			</p>
			<p>
			<form action="/bitrix/admin/oauth2_server_clients_list.php?lang=<?=LANGUAGE_ID?>" method="POST" onSubmit="return confirm('<?=GetMessage('OAUTH2_SERVER_APP_DELETE_QUESTION', array('#CLIENT_ID#' => $row['client_id']))?>');">
				<?=bitrix_sessid_post()?>
				<input type="hidden" name="action" value="delete">
				<input type="hidden" name="client_id" value="<?=$row['client_id']?>">
				<input type="submit" class="adm-btn adm-btn-delete" value="<?=GetMessage('OAUTH2_SERVER_APP_DELETE')?>">
			</form>
			</p>
		</td>
	</tr>
	<tr class="adm-list-table-row">
		<td class="adm-list-table-cell">
			Client Secret
		</td>
		<td class="adm-list-table-cell">
			<?=$row['client_secret']?>
		</td>
	</tr>
	<tr class="adm-list-table-row">
		<td class="adm-list-table-cell">
			<?=GetMessage('OAUTH2_SERVER_APP_REDIRECT_URI')?>
		</td>
		<td class="adm-list-table-cell">
			<?=($row <> '' ? $row['redirect_uri'] : '&nbsp')?>
		</td>
	</tr>
	<tr class="adm-list-table-row">
		<td class="adm-list-table-cell">
			<?=GetMessage('OAUTH2_SERVER_APP_SCOPES')?>
		</td>
		<td class="adm-list-table-cell">
			<?=implode(', ', explode(' ', $row['scope']))?>
		</td>
	</tr>
	<? if ($i<>count($appList)) {?>
	<tr class="adm-list-table-row">
		<td class="adm-list-table-cell" colspan=3>
			<hr>
		</td>
	</tr>
	<?}
	};?>
</table>

<p>
<a href="/bitrix/admin/oauth2_server_clients_add_edit.php?lang=<?=LANGUAGE_ID?>&do=add" class="adm-btn adm-btn-save adm-btn-add"><?=GetMessage('OAUTH2_SERVER_APP_ADD')?></a>
</p>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>