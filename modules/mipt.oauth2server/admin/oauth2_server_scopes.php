<?
error_reporting(E_ALL);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mipt.oauth2server/include.php");
IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mipt.oauth2server");
if ($POST_RIGHT == "D"){
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));}
?>

<?
$APPLICATION->SetTitle(GetMessage('OAUTH2_SERVER_SCOPES_TITLE'));
global $DB;

if ($REQUEST_METHOD == 'POST' && $POST_RIGHT == 'W' && $_REQUEST['action'] == 'save' &&  check_bitrix_sessid()) {
	$sqlRes = $DB->Query('SELECT `scope` FROM `oauth_scopes`');
	$scopes = $_REQUEST['scope'];
	while ($row = $sqlRes->Fetch()) {
		if(!$DB->Query('UPDATE `oauth_scopes` SET `is_default`='.(in_array($row['scope'], $scopes) ? '1' : 'null').' WHERE `scope`=\''.$row['scope'].'\'')) {
		LocalRedirect($APPLICATION->GetCurPage().'?lang='.LANGUAGE_ID.'&mess=savecant');
		}
	}
		LocalRedirect($APPLICATION->GetCurPage().'?lang='.LANGUAGE_ID.'&mess=saveok');
}

$sqlRes = $DB->Query('SELECT * FROM `oauth_scopes`');

$scopesList = array();
while ($row = $sqlRes->Fetch()) {
	array_push($scopesList, $row);
}
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
if ($_REQUEST['mess'] <> '') {
	$messtypes = array(
		"saveok"   =>    "OK".GetMessage('OAUTH2_SERVER_SCOPES_SAVE_OK'),
		"savecant" => "ERROR".GetMessage('OAUTH2_SERVER_SCOPES_SAVE_CANT'),
	);
	if (array_key_exists($_REQUEST['mess'], $messtypes)) {
		if (substr($messtypes[$_REQUEST['mess']], 0, 2) == "OK") {
			$TYPE = "OK";
			$messtypes[$_REQUEST['mess']] = substr($messtypes[$_REQUEST['mess']], 2);
		} elseif (substr($messtypes[$_REQUEST['mess']], 0, 5) == "ERROR") {
			$TYPE = "ERROR";
			$messtypes[$_REQUEST['mess']] = substr($messtypes[$_REQUEST['mess']], 5);
		}
		CAdminMessage::ShowMessage(array("MESSAGE"=>$messtypes[$_REQUEST['mess']], "TYPE"=>$TYPE));
	}
}
?>

<form action="<?=$APPLICATION->GetCurPage()?>" method="POST" onSubmit="return confirm('<?=GetMessage('OAUTH2_SERVER_SCOPES_SAVE_QUESTION')?>');">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">
	<input type="hidden" name="action" value="save">
	<table class="adm-list-table">
		<tr class="adm-list-table-header">
			<td class="adm-list-table-cell">
				<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_SCOPES_HEADER_NAME_TITLE');?></div>
			</td>
			<td class="adm-list-table-cell">
				<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_SCOPES_HEADER_ISDEFAULT_TITLE');?></div>
			</td>
			<td class="adm-list-table-cell">
				<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_SCOPES_HEADER_DESC_TITLE');?></div>
			</td>
		</tr>
		<?
		foreach ($scopesList as $row) {
		?>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">
				<?=$row['scope']?>
			</td>
			<td class="adm-list-table-cell">
				<input type="checkbox" name="scope[]" value="<?=$row['scope']?>" <?=(intval($row['is_default']) ? 'checked' : '')?> />
			</td>
			<td class="adm-list-table-cell">
				<?=GetMessage('OAUTH2_SERVER_SCOPES_DESC_'.strtoupper($row['scope']))?>
			</td>
		</tr>
		<?};?>
	</table>
	<p>
		<input type="submit" class="adm-btn-green" value="<?=GetMessage('OAUTH2_SERVER_SCOPES_SAVE')?>" />
	</p>
</form>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>