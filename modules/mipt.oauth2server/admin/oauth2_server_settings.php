<?
error_reporting(E_ALL);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mipt.oauth2server/include.php");
IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mipt.oauth2server");
if ($POST_RIGHT == "D"){
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));}
?>

<?
function BreakCSS($css)	{
    $results = array();
    preg_match_all('/(.+?)\s?\{\s?(.+?)\s?\}/', $css, $matches);
    foreach($matches[0] AS $i=>$original)
        foreach(explode(';', $matches[2][$i]) AS $attr)
            if (strlen(trim($attr)) > 0) // for missing semicolon on last element, which is legal
            {
                list($name, $value) = explode(':', $attr);
                $results[$matches[1][$i]][trim($name)] = trim($value);
            }
    return $results;
}

function rgbToHsl( $r, $g, $b ) {
	$oldR = $r;
	$oldG = $g;
	$oldB = $b;

	$r /= 255;
	$g /= 255;
	$b /= 255;

    $max = max( $r, $g, $b );
	$min = min( $r, $g, $b );

	$h;
	$s;
	$l = ( $max + $min ) / 2;
	$d = $max - $min;

    	if( $d == 0 ){
        	$h = $s = 0; // achromatic
    	} else {
        	$s = $d / ( 1 - abs( 2 * $l - 1 ) );

		switch( $max ){
	            case $r:
	            	$h = 60 * fmod( ( ( $g - $b ) / $d ), 6 ); 
                        if ($b > $g) {
	                    $h += 360;
	                }
	                break;

	            case $g: 
	            	$h = 60 * ( ( $b - $r ) / $d + 2 ); 
	            	break;

	            case $b: 
	            	$h = 60 * ( ( $r - $g ) / $d + 4 ); 
	            	break;
	        }			        	        
	}
	return array('H' => round( $h, 2 ), 'S' => round( $s, 2 ), 'L' => round( $l, 2 ));
}

function hslToRgb( $h, $s, $l ){
    $r; 
    $g; 
    $b;

	$c = ( 1 - abs( 2 * $l - 1 ) ) * $s;
	$x = $c * ( 1 - abs( fmod( ( $h / 60 ), 2 ) - 1 ) );
	$m = $l - ( $c / 2 );

	if ( $h < 60 ) {
		$r = $c;
		$g = $x;
		$b = 0;
	} else if ( $h < 120 ) {
		$r = $x;
		$g = $c;
		$b = 0;			
	} else if ( $h < 180 ) {
		$r = 0;
		$g = $c;
		$b = $x;					
	} else if ( $h < 240 ) {
		$r = 0;
		$g = $x;
		$b = $c;
	} else if ( $h < 300 ) {
		$r = $x;
		$g = 0;
		$b = $c;
	} else {
		$r = $c;
		$g = 0;
		$b = $x;
	}

	$r = ( $r + $m ) * 255;
	$g = ( $g + $m ) * 255;
	$b = ( $b + $m  ) * 255;

    return array('R' => floor( $r ), 'G' => floor( $g ), 'B' => floor( $b ));
}

$APPLICATION->SetTitle(GetMessage('OAUTH2_SERVER_SETTINGS_TITLE'));
global $DB;

if ($REQUEST_METHOD == 'POST' && $POST_RIGHT == 'W' && $_REQUEST['action'] == 'save' &&  check_bitrix_sessid()) {
	$sqlRes = $DB->Query('SELECT `name`, `type` FROM `oauth_settings`');
	$sqlQ = 'UPDATE `oauth_settings` SET `value` = CASE `name` ';
	$color = '';
	$settingsList = array();
	while ($row = $sqlRes->Fetch()) {
		$val = '';
		if ($row['type'] == 'checkbox') {
			$val = (htmlspecialcharsbx($_REQUEST[$row['name']]) == 'yes' ? 'yes' : 'no');
		}
		elseif ($row['type'] == 'colorpicker') {
			$val = $color = htmlspecialcharsbx(substr($_REQUEST[$row['name']], 1));
		}
		else {
			$val = $DB->ForSql($_REQUEST[$row['name']]);
		}
		$sqlQ .= 'WHEN \''.$row['name'].'\' THEN \''.$val.'\' ';
		$settingsList[] = $row['name'];
	}
	$sqlQ .= 'END';
	$eol = CAllEvent::GetMailEOL();
	$tab = chr(9);
	$style = preg_replace('/'.$eol.'/', '', file_get_contents($_SERVER['DOCUMENT_ROOT']."/oauth/style.css"));
	$style = BreakCSS($style);
	if (strlen($color) == 3) {
		$color2['R'] = hexdec(substr($color, 0, 1));
		$color2['G'] = hexdec(substr($color, 1, 1));
		$color2['B'] = hexdec(substr($color, 2, 1));
	}
	elseif (strlen($color) == 6) {
		$color2['R'] = hexdec(substr($color, 0, 2));
		$color2['G'] = hexdec(substr($color, 2, 2));
		$color2['B'] = hexdec(substr($color, 4, 2));
	}
	$colordown = $colorup = rgbToHsl($color2['R'], $color2['G'], $color2['B']);
	$colordown['L'] = ($colordown['L'] <= 0.1 ? 0 : $colordown['L'] - 0.1);
	$colorup['L'] = ($colorup['L'] >= 0.9 ? 1 : $colorup['L'] + 0.1);
	$colordown = hslToRgb($colordown['H'], $colordown['S'], $colordown['L']);
	$colorup = hslToRgb($colorup['H'], $colorup['S'], $colorup['L']);
	$colordown = '#'.str_pad(dechex($colordown['R']), 2, "0", STR_PAD_LEFT).str_pad(dechex($colordown['G']), 2, "0", STR_PAD_LEFT).str_pad(dechex($colordown['B']), 2, "0", STR_PAD_LEFT);
	$colorup = '#'.str_pad(dechex($colorup['R']), 2, "0", STR_PAD_LEFT).str_pad(dechex($colorup['G']), 2, "0", STR_PAD_LEFT).str_pad(dechex($colorup['B']), 2, "0", STR_PAD_LEFT);
	$color = '#'.$color;
	$style['.logoutbutton']['background'] = $style['.logoutbutton:hover']['color'] = $style['.head']['background'] = $style['span.appname']['color'] = $style['.btnaccess']['background'] = $style['.btncancel']['color'] = $color;
	$style['.btnaccess']['border'] = $style['.btncancel']['border'] = $colordown;
	$style['.bottombuttons button:hover, input[type=\'submit\']:hover']['background'] = $style['.bottombuttons a']['color'] = $colorup;
	$tostyle = '';
	foreach ($style as $selector => $properties) {
		$tostyle .= $selector.' {'.$eol.$tab;
		foreach ($properties as $property => $value) {
			$tostyle .= $property.': '.$value.';'.$eol.$tab;
		}
		$tostyle = substr($tostyle, 0, -1).'}'.$eol;
	}
	file_put_contents($_SERVER['DOCUMENT_ROOT']."/oauth/style.css", $tostyle);
	if(!$DB->Query($sqlQ)) {
		LocalRedirect($APPLICATION->GetCurPage().'?lang='.LANGUAGE_ID.'&mess=savecant');
	}
	LocalRedirect($APPLICATION->GetCurPage().'?lang='.LANGUAGE_ID.'&mess=saveok');
}

$sqlRes = $DB->Query('SELECT * FROM `oauth_settings`');

$settings = array();
while ($row = $sqlRes->Fetch()) {
	array_push($settings, $row);
}
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
if ($_REQUEST['mess'] <> '') {
	$messtypes = array(
		"saveok"   =>    "OK".GetMessage('OAUTH2_SERVER_SETTINGS_SAVE_OK'),
		"savecant" => "ERROR".GetMessage('OAUTH2_SERVER_SETTINGS_SAVE_CANT'),
	);
	if (array_key_exists($_REQUEST['mess'], $messtypes)) {
		if (substr($messtypes[$_REQUEST['mess']], 0, 2) == "OK") {
			$TYPE = "OK";
			$messtypes[$_REQUEST['mess']] = substr($messtypes[$_REQUEST['mess']], 2);
		} elseif (substr($messtypes[$_REQUEST['mess']], 0, 5) == "ERROR") {
			$TYPE = "ERROR";
			$messtypes[$_REQUEST['mess']] = substr($messtypes[$_REQUEST['mess']], 5);
		}
		CAdminMessage::ShowMessage(array("MESSAGE"=>$messtypes[$_REQUEST['mess']], "TYPE"=>$TYPE));
	}
}
?>

<form action="<?=$APPLICATION->GetCurPage()?>" method="POST" onSubmit="return confirm('<?=GetMessage('OAUTH2_SERVER_SETTINGS_SAVE_QUESTION')?>');">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">
	<input type="hidden" name="action" value="save">
	<table class="adm-list-table">
		<tr class="adm-list-table-header">
			<td class="adm-list-table-cell">
				<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_SETTINGS_HEADER_NAME_TITLE');?></div>
			</td>
			<td class="adm-list-table-cell">
				<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_SETTINGS_HEADER_VALUE_TITLE');?></div>
			</td>
		</tr>
		<?
		foreach ($settings as $row) {
			switch ($row['type']) {
				case 'checkbox':
					$slabel  = '<label>';
					$slabele = GetMessage('OAUTH2_SERVER_SETTINGS_ENABLE').'</label>';
					$stype   = 'checkbox';
					$sname   = $row['name'];
					$sval    = 'yes';
					$sparams = ($row['value'] == 'yes' ? 'checked ' : '');
					break;

				case 'text':
					$slabel  = '';
					$slabele = '';
					$stype   = 'text';
					$sname   = $row['name'];
					$sval    = htmlspecialcharsbx($row['value']);
					$sparams = '';
					break;

				case 'colorpicker':
					$slabel  = '';
					$slabele = '';
					$stype   = 'color';
					$sname   = $row['name'];
					$sval    = '#'.$row['value'];
					$sparams = '';
					break;
				
				default:
					$slabel  = '';
					$slabele = '';
					$stype   = 'text';
					$sname   = $row['name'];
					$sval    = htmlspecialcharsbx($row['value']);
					$sparams = '';
					break;
			}
		?>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">
				<b><?=GetMessage('OAUTH2_SERVER_SETTINGS_'.strtoupper($row['name'].'_NAME'))?></b><br>
				<?=GetMessage('OAUTH2_SERVER_SETTINGS_'.strtoupper($row['name'].'_DESC'))?>
			</td>
			<td class="adm-list-table-cell">
				<?=$slabel?><input type="<?=$stype?>" name="<?=$sname?>" value="<?=$sval?>" <?=$sparams?> /><?=$slabele?>
			</td>
		</tr>
		<?};?>
	</table>
	<p>
		<input type="submit" class="adm-btn-green" value="<?=GetMessage('OAUTH2_SERVER_SETTINGS_SAVE')?>" />
	</p>
</form>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>