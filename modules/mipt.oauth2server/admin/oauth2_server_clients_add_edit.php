<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mipt.oauth2server/include.php");
IncludeModuleLangFile(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mipt.oauth2server");
if ($POST_RIGHT == "D"){
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));}
?>

<?
global $DB;

$appinfo = array();
$scopes = array();
$granttypes = array(
	'authorization_code',
	'client_credentials',
);

$sqlRes = $DB->Query("SELECT * FROM `oauth_scopes`");
while ($row = $sqlRes->Fetch()) {
	array_push($scopes, $row);
}

$do = htmlspecialcharsbx($_REQUEST['do']);

if ($do == 'add') {
	$APPLICATION->SetTitle(GetMessage('OAUTH2_SERVER_APPADDEDIT_ADD_TITLE'));
}
elseif ($do == 'edit' && $_REQUEST['client_id'] <> '') {
	$APPLICATION->SetTitle(GetMessage('OAUTH2_SERVER_APPADDEDIT_EDIT_TITLE').htmlspecialcharsbx($_REQUEST['client_id']));
	$sqlRes = $DB->Query("SELECT * FROM `oauth_clients` WHERE client_id = '".$DB->ForSql($_REQUEST['client_id'])."'");
	if (intval($sqlRes->AffectedRowsCount())>0) {
		$appinfo = $sqlRes->Fetch();
	}
	else {
		LocalRedirect("/bitrix/admin/oauth2_server_clients_list.php?lang=".LANGUAGE_ID."&mess=editcant&client_id=".htmlspecialcharsbx($_REQUEST['client_id']));
	}
}
else {
	if ($REQUEST_METHOD == 'POST' && $POST_RIGHT == 'W' && $_REQUEST['action'] == 'add' && check_bitrix_sessid()) {
		$arFields = array (
			'client_id' => trim(htmlspecialcharsbx($_REQUEST['client_id'])),
			'client_secret' => trim(htmlspecialcharsbx($_REQUEST['client_secret'])),
			'redirect_uri' => trim(htmlspecialcharsbx($_REQUEST['redirect_uri'])),
			'grant_types' => trim(htmlspecialcharsbx($_REQUEST['grant_types'])),
			'scope' => htmlspecialcharsbx(implode(' ', $_REQUEST['scope']))
		);
		if ($_REQUEST['client_id'] <> '' && preg_match('/^[0-9A-Za-z]{64}$/', $_REQUEST['client_secret']) && $_REQUEST['grant_types'] <> '') {
			if (intval($DB->Query('SELECT * FROM `oauth_clients` WHERE client_id = \''.$DB->ForSql($_REQUEST['client_id']).'\'')->AffectedRowsCount()) == 0) {
				$cnt1 = array_shift($DB->Query('SELECT count(*) FROM `oauth_clients`')->Fetch());
				$DB->Add('oauth_clients', $arFields);
				$cnt2 = array_shift($DB->Query('SELECT count(*) FROM `oauth_clients`')->Fetch());
				if($cnt2 == $cnt1+1) {
					LocalRedirect("/bitrix/admin/oauth2_server_clients_list.php?lang=".LANGUAGE_ID."&mess=addok&client_id=".htmlspecialcharsbx($_REQUEST['client_id']));
				} else {
					LocalRedirect("/bitrix/admin/oauth2_server_clients_list.php?lang=".LANGUAGE_ID."&mess=addcant&client_id=".htmlspecialcharsbx($_REQUEST['client_id']));
				}
			}
			else {
				LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANGUAGE_ID."&do=add&mess=addcantexist&".http_build_query($arFields));
			}
		}
		else {
			LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANGUAGE_ID."&do=add&mess=emptyfields&".http_build_query($arFields));
		}
	} elseif ($REQUEST_METHOD == 'POST' && $POST_RIGHT == 'W' && $_REQUEST['action'] == 'edit' && $_REQUEST['client_id'] <> '' && check_bitrix_sessid()) {
		$arFields = array (
			'client_id' => trim(htmlspecialcharsbx($_REQUEST['client_id'])),
			'client_secret' => trim(htmlspecialcharsbx($_REQUEST['client_secret'])),
			'redirect_uri' => trim(htmlspecialcharsbx($_REQUEST['redirect_uri'])),
			'grant_types' => trim(htmlspecialcharsbx($_REQUEST['grant_types'])),
			'scope' => htmlspecialcharsbx(implode(' ', $_REQUEST['scope']))
		);
		if ($_REQUEST['client_id'] <> '' && preg_match('/^[0-9A-Za-z]{64}$/', $_REQUEST['client_secret']) && $_REQUEST['grant_types'] <> '') {
			if (intval($DB->Query('SELECT * FROM `oauth_clients` WHERE client_id = \''.$DB->ForSql($_REQUEST['client_id']).'\'')->AffectedRowsCount()) == 1) {
				$strUpdate = $DB->PrepareUpdate('oauth_clients', $arFields);
				if ($strUpdate != '') {
					if (!$DB->Query('UPDATE `oauth_clients` SET '.$strUpdate.' WHERE `client_id`=\''.$arFields['client_id'].'\'')) {
						LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANGUAGE_ID."&do=edit&mess=updatecant".http_build_query($arFields));
					}
				}
				LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANGUAGE_ID."&do=edit&mess=updateok&client_id=".$arFields['client_id']);
			}
			else {
				LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANGUAGE_ID."&do=edit&mess=updatecantnotexist&".http_build_query($arFields));
			}
		}
		else {
			LocalRedirect($APPLICATION->GetCurPage()."?lang=".LANGUAGE_ID."&do=edit&mess=emptyfields&".http_build_query($arFields));
		}
	} else {
		LocalRedirect("/bitrix/admin/oauth2_server_clients_list.php?lang=".LANGUAGE_ID);
	}
}
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<?
if ($_REQUEST['mess'] <> '' && $_REQUEST['client_id'] <> '') {
	$messtypes = array(
		"addcantexist"       => "ERROR".GetMessage('OAUTH2_SERVER_APP_ADD_CANT_EXIST', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
		"emptyfields"        => "ERROR".GetMessage('OAUTH2_SERVER_APPINFO_EMPTY_FIELDS'),
		"updatecantnotexist" => "ERROR".GetMessage('OAUTH2_SERVER_APPINFO_CANT_UPDATE_NOT_EXIST', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
		"updatecant"         => "ERROR".GetMessage('OAUTH2_SERVER_APPINFO_CANT_UPDATE', array('#CLIENT_ID#' => htmlspecialcharsbx($_REQUEST['client_id']))),
		"updateok"           =>    "OK".GetMessage('OAUTH2_SERVER_APPINFO_UPDATE_OK'),
	);
	if (array_key_exists($_REQUEST['mess'], $messtypes)) {
		if (substr($messtypes[$_REQUEST['mess']], 0, 2) == "OK") {
			$TYPE = "OK";
			$messtypes[$_REQUEST['mess']] = substr($messtypes[$_REQUEST['mess']], 2);
		} elseif (substr($messtypes[$_REQUEST['mess']], 0, 5) == "ERROR") {
			$TYPE = "ERROR";
			$messtypes[$_REQUEST['mess']] = substr($messtypes[$_REQUEST['mess']], 5);
		}
		CAdminMessage::ShowMessage(array("MESSAGE"=>$messtypes[$_REQUEST['mess']], "TYPE"=>$TYPE));
	}
}
?>
<script type="text/javascript">
	var genchars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	
	
	function genpass(passlen, q) {
		if ((q == 'Y' && confirm('<?=GetMessage('OAUTH2_SERVER_APPADDEDIT_GENSECRET_QUESTION')?>')) || (q == 'N')) {
			genpassword='';
			for ( i=0; i<passlen; i++ )
				genpassword += genchars.charAt(Math.floor(Math.random()*genchars.length));

			document.getElementById('clsecret').value = genpassword;
		}
	}
</script>

<form action="<?=$APPLICATION->GetCurPage()?>" method="POST" <?=($do == 'edit' ? 'onSubmit="return confirm(\''.GetMessage('OAUTH2_SERVER_APPINFO_EDIT_QUESTION').'\')"' : '')?>>
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">
	<input type="hidden" name="action" value="<?=$do?>">
	<table class="adm-list-table">
		<tr class="adm-list-table-header">
			<td class="adm-list-table-cell">
				<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_APPINFO_HEADER_PARAM_TITLE');?></div>
			</td>
			<td class="adm-list-table-cell">
				<div class="adm-list-table-cell-inner"><?=GetMessage('OAUTH2_SERVER_APPINFO_HEADER_VALUE_TITLE');?></div>
			</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">
				<b>Client ID</b>
			</td>
			<td class="adm-list-table-cell">
				<?if ($do == 'add') {?>
					<input type="text" name="client_id" value="<?=htmlspecialcharsbx($_REQUEST['client_id'])?>" pattern="^\S.*\S$" required>
				<?} elseif ($do == 'edit') {?>
					<?=$appinfo['client_id']?>
					<input type="hidden" name="client_id" value="<?=$appinfo['client_id']?>" />
				<?}?>
			</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">
				<b>Client Secret</b>
				<br />
				<?=GetMessage('OAUTH2_SERVER_APPINFO_SECRET_DESC')?>
			</td>
			<td class="adm-list-table-cell">
				<input type="text" id="clsecret" name="client_secret" value="<?=($do == 'edit' ? ($_REQUEST['client_secret'] ? $_REQUEST['client_secret'] : $appinfo['client_secret']) : $_REQUEST['client_secret'])?>" size="70" pattern="[0-9A-Za-z]{64}" required />
				<input type="button" class="adm-btn-green" value="<?=GetMessage('OAUTH2_SERVER_APPADDEDIT_GENSECRET')?>" onClick="genpass(64, '<?=($do == 'add' ? 'N' : 'Y')?>');" />
			</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">
				<b><?=GetMessage('OAUTH2_SERVER_APPINFO_REDIRECT_URI')?></b>
				<br />
				<?=GetMessage('OAUTH2_SERVER_APPINFO_FIELD_MAY_BE_EMPTY')?>
			</td>
			<td class="adm-list-table-cell">
				<input type="text" name="redirect_uri" value="<?=($do == 'edit' ? ($_REQUEST['redirect_uri'] ? $_REQUEST['redirect_uri'] : $appinfo['redirect_uri']) : $_REQUEST['redirect_uri'])?>" />
			</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">
				<b><?=GetMessage('OAUTH2_SERVER_APPINFO_SCOPES')?></b>
			</td>
			<td class="adm-list-table-cell">
				<?if ($do == 'add') {
					foreach ($scopes as $value) {?>
						<label><input type="checkbox" name="scope[]" value="<?=$value['scope']?>" <?=($_REQUEST['client_id'] == '' ? ((!!$value['is_default']) == true ? 'checked' : '') : (in_array($value['scope'], explode(' ', $_REQUEST['scope'])) ? 'checked' : ''))?> /><?=$value['scope']?></label><br />
					<?}
				} elseif ($do == 'edit') {
					foreach ($scopes as $value) {?>
					<label><input type="checkbox" name="scope[]" value="<?=$value['scope']?>" <?=(in_array($value['scope'], explode(' ', ($_REQUEST['scope'] ? $_REQUEST['scope'] : $appinfo['scope']))) ? 'checked' : '')?> /><?=$value['scope']?></label><br />
					<?}
				}?>
			</td>
		</tr>
		<tr class="adm-list-table-row">
			<td class="adm-list-table-cell">
				<b><?=GetMessage('OAUTH2_SERVER_APPINFO_GRANT_TYPE')?></b>
			</td>
			<td class="adm-list-table-cell">
				<select name="grant_types" required>
					<?foreach ($granttypes as $value) {?>
						<option value="<?=$value?>" <?=($do == 'edit' ? (($_REQUEST['grant_types'] ? $_REQUEST['grant_types'] : $appinfo['grant_types'])==$value ? 'selected' : '') : ($_REQUEST['grant_types']==$value ? 'selected' : ''))?>><?=$value?></option>
					<?}?>
				</select>
			</td>
		</tr>
	</table>
	<p>
		<?if ($do == 'add') {?>
			<input type="submit" class="adm-btn-green" value="<?=GetMessage('OAUTH2_SERVER_APP_ADD')?>" />
		<?} elseif ($do == 'edit') {?>
			<input type="submit" class="adm-btn-green" value="<?=GetMessage('OAUTH2_SERVER_APPINFO_SAVE')?>" />
		<?}?>
	</p>
</form>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>