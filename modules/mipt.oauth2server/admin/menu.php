<?
IncludeModuleLangFile(__FILE__);
$MOD_RIGHT = $APPLICATION->GetGroupRight("mipt.oauth2server");
if($MOD_RIGHT!="D")
{
    $aMenu[] = array(
        "parent_menu" => "global_menu_settings",
        "section"     => "mipt.oauth2server",
        "icon"        => "oauth2_menu_icon",
        "page_icon"   => "oauth2_page_icon",
        "sort"        => 500,
        "text"        => "OAuth2",
        "title"       => GetMessage("OAUTH2_MENU_TITLE"),
        "more_url"    => array(),
        "items_id"    => "menu_oauth2",
        "items"       => array (
            array (
                "text"        => GetMessage('OAUTH2_MENU_SERVER_CLIENTS'),
                "url"         => "/bitrix/admin/oauth2_server_clients_list.php?lang=".LANGUAGE_ID,
                "title"       => GetMessage('OAUTH2_MENU_SERVER_CLIENTS_TITLE'),
                "more_url"    => array('oauth2_server_clients_add_edit.php'),
                "module_id"   => "mipt.oauth2server",
            ),
            array (
                "text"      => GetMessage('OAUTH2_MENU_SERVER_SCOPES'),
                "url"       => "/bitrix/admin/oauth2_server_scopes.php?lang=".LANGUAGE_ID,
                "title"     => GetMessage('OAUTH2_MENU_SERVER_SCOPES_TITLE'),
                "more_url"  => array(),
                "module_id" => "mipt.oauth2server",
            ),
            array (
                "text"      => GetMessage('OAUTH2_MENU_SERVER_SETTINGS'),
                "url"       => "/bitrix/admin/oauth2_server_settings.php?lang=".LANGUAGE_ID,
                "title"     => GetMessage('OAUTH2_MENU_SERVER_SETTINGS_TITLE'),
                "more_url"  => array(),
                "module_id" => "mipt.oauth2server",
            ),
        )
    );
    return $aMenu;
}
return false;
?>