<?

$MESS['PAYONLINE_SETTINGS_TAB_NAME'] = 'PayOnline System';
$MESS['PAYONLINE_SETTINGS_TAB_TITLE'] = 'PayOnline System Settings';
$MESS['PAYONLINE_PAY_SYSTEM_TAB_NAME'] = 'Pay System';
$MESS['PAYONLINE_PAY_SYSTEM_TAB_TITLE'] = 'New Pay System Settings';

$MESS['PAYONLINE_MERCHANT_ID_TITLE'] = 'Merchant ID';
$MESS['PAYONLINE_MERCHANT_ID_DESCRIPTION'] = 'From PayOnline System account information';
$MESS['PAYONLINE_SECURITY_KEY_TITLE'] = 'Security key';
$MESS['PAYONLINE_SECURITY_KEY_DESRIPTION'] = 'From PayOnline System account information';
$MESS['PAYONLINE_ACTIVE_TITLE'] = 'Activity';
$MESS['PAYONLINE_SITE_TITLE'] = 'Site';
$MESS['PAYONLINE_PAY_SYSTEM_TITLE'] = 'Name';
$MESS['PAYONLINE_PAY_SYSTEM_DESCRIPTION'] = 'Description';
$MESS['PAYONLINE_PAYER_TYPES_TITLE'] = 'Payer types';

$MESS['PAYONLINE_INSTALLATION_NOTE'] = 'Besides the above-stated options at creation of new payment system following parametres of the output agent will be set:
<ul>
	<li><b>The address of the form of payment or a choice of the payment tool:</b> https://secure.payonlinesystem.com/ru/payment/select/;</li>
	<li><b>The feedback address:</b> http://домен_сайта/bitrix/admin/payonline_callback.php;</li>
	<li><b>Absolute URL the address on which the payer after payment end will be sent:</b> http://домен_сайта/payonline/;</li>
	<li><b>The maximum term of payment from the moment of order registration:</b> 720 hours (30 days);</li>
	<li><b>To make transaction cancellation at order cancellation:</b> yes;</li>
	<li><b>To make the refund on successful transaction at order cancellation:</b> yes.</li>
</ul>
All parametres will be accessible to change on page of editing of the created payment system after module installation.';