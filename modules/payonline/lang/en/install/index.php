<?
$MESS['PAYONLINE_INSTALL_NAME'] = 'PayOnline System';
$MESS['PAYONLINE_INSTALL_DESCRIPTION'] = 'Module for payments through PayOnline System processing center';
$MESS['PAYONLINE_INSTALL_TITLE'] = 'PayOnline System Module Installation';
$MESS['PAYONLINE_UNINSTALL_TITLE'] = 'PayOnline System Module Uninstallation';