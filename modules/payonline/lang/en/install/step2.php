<?

$MESS['PAYONLINE_INSTALLATION_SUCCESSFULL'] = 'Module was successfully installed';
$MESS['PAYONLINE_INSTALLATION_SUCCESSFULL_NOTE'] = 'At module installation the new payment system with use of the output agent &laquo;Payment through PayOnline System&raquo; has been created.<br />
<a href="/bitrix/admin/sale_pay_system_edit.php?ID=<?=$pay_system_id?>&amp;lang=<?=LANG?>">To change set at installation of the module of adjustment it is possible here</a>.';
$MESS['PAYONLINE_INSTALLATION_ERROR'] = 'There were errors during install';
$MESS['PAYONLINE_RETURN_BACK'] = 'Return back';