<?

$MESS['PAYONLINE_TITLE'] = 'Payment using PayOnline System';
$MESS['PAYONLINE_DESCRIPTION'] = 'Payment using PayOnline System';

$MESS['PAYONLINE_MERCHANT_ID_NAME'] = 'Merchant ID';
$MESS['PAYONLINE_MERCHANT_ID_DESCR'] = 'From PayOnline System account information';
$MESS['PAYONLINE_SECURITY_KEY_NAME'] = 'Security key';
$MESS['PAYONLINE_SECURITY_KEY_DESCR'] = 'From PayOnline System account information';
$MESS['PAYONLINE_PAYMENT_URL_NAME'] = 'Payment URL';
$MESS['PAYONLINE_PAYMENT_URL_DESCR'] = 'Possible values:<br /><strong>https://secure.payonlinesystem.com/ru/payment/select/</strong> - the form of a choice of the payment tool;<br /><strong>https://secure.payonlinesystem.com/ru/payment/select/qiwi/</strong> - the payment form through QIWI;<br /><strong>https://secure.payonlinesystem.com/ru/payment/select/webmoney/</strong> - the payment form through WebMoney;<br /><strong>https://secure.payonlinesystem.com/ru/payment/</strong> - the form of payment from a bank card.';
$MESS['PAYONLINE_CALL_BACK_URL_NAME'] = 'Callback URL';
$MESS['PAYONLINE_CALL_BACK_URL_DESCR'] = 'Resource on your site which will be automatically caused by system in case of successful passage of payment. The address can include parametres, for example <a href="http://hostname/confirm.php?from=PayOnlineSystem">http://hostname/confirm.php?from=PayOnlineSystem</a>. Address adjustment is supposed only on 80 port under report HTTP or HTTPS. For work HTTPS of the report on party TSP should be валидный SSL the certificate.';
$MESS['PAYONLINE_RETURN_URL_NAME'] = 'Absolute URL on which the payer after payment end will be sent';
$MESS['PAYONLINE_ORDER_ID_NAME'] = 'Order ID';
$MESS['PAYONLINE_SHOULD_PAY_NAME'] = 'Should pay';
$MESS['PAYONLINE_CURRENCY_NAME'] = 'Currency';
$MESS['PAYONLINE_LOCATION_COUNTRY_NAME'] = 'Country';
$MESS['PAYONLINE_LOCATION_CITY_NAME'] = 'City';
$MESS['PAYONLINE_ADDRESS_NAME'] = 'Address (without city)';
$MESS['PAYONLINE_INDEX_NAME'] = 'Zip code';
$MESS['PAYONLINE_PHONE_NAME'] = 'Phone number';
$MESS['PAYONLINE_EMAIL_NAME'] = 'E-mail';
$MESS['PAYONLINE_VALID_UNTIL_NAME'] = 'Срок платежа';
$MESS['PAYONLINE_VALID_UNTIL_NAME'] = 'Valid until';
$MESS['PAYONLINE_VALID_UNTIL_DESCR'] = 'The maximum term of payment from the moment of order registration (in hours).<br />If to leave empty, term will not be established';
$MESS['PAYONLINE_CAN_VOID_NAME'] = 'To make transaction cancellation at order cancellation?';
$MESS['PAYONLINE_CAN_VOID_DESCR'] = 'Possible values:<br /><strong>Y</strong> - yes;<br /><strong>N</strong> - no.<br />Transaction cancellation is possible within 24 hours from the moment of payment passage.';
$MESS['PAYONLINE_CAN_REFUND_NAME'] = 'To make the refund on successful transaction at order cancellation?';
$MESS['PAYONLINE_CAN_REFUND_DESCR'] = 'Possible values:<br /><strong>Y</strong> - yes;<br /><strong>N</strong> - no.<br />The refund is possible in 24 hours from the moment of passage of payment and till 180 days.';