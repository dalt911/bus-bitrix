<?

$MESS['PAYONLINE_INSTALLATION_SUCCESSFULL'] = 'Модуль успешно установлен';
$MESS['PAYONLINE_INSTALLATION_SUCCESSFULL_NOTE'] = 'При установке модуля была создана новая платежная система с использованием обработчика &laquo;Оплата через PayOnline System&raquo;.<br />
<a href="/bitrix/admin/sale_pay_system_edit.php?ID=<?=$pay_system_id?>&amp;lang=<?=LANG?>">Изменить заданные при установке модуля настройки можно здесь</a>.';
$MESS['PAYONLINE_INSTALLATION_ERROR'] = 'В процессе установки возникла ошибка';
$MESS['PAYONLINE_RETURN_BACK'] = 'Вернуться назад';