<?
$MESS['PAYONLINE_INSTALL_NAME'] = 'Платежная система PayOnline System';
$MESS['PAYONLINE_INSTALL_DESCRIPTION'] = 'Модуль оплаты с помощью процессингового центра PayOnline System';
$MESS['PAYONLINE_INSTALL_TITLE'] = 'Установка модуля PayOnline System';
$MESS['PAYONLINE_UNINSTALL_TITLE'] = 'Удаление модуля PayOnline System';