<?

$MESS['PAYONLINE_SETTINGS_TAB_NAME'] = 'PayOnline System';
$MESS['PAYONLINE_SETTINGS_TAB_TITLE'] = 'Настройки PayOnline System';
$MESS['PAYONLINE_PAY_SYSTEM_TAB_NAME'] = 'Платежная система';
$MESS['PAYONLINE_PAY_SYSTEM_TAB_TITLE'] = 'Настройки новой платежной системы';

$MESS['PAYONLINE_MERCHANT_ID_TITLE'] = 'Номер учетной записи';
$MESS['PAYONLINE_MERCHANT_ID_DESCRIPTION'] = '<strong>Merchant ID.</strong> Доступен в личном кабинете PayOnline System';
$MESS['PAYONLINE_SECURITY_KEY_TITLE'] = 'Секретный ключ';
$MESS['PAYONLINE_SECURITY_KEY_DESRIPTION'] = '<strong>Security key.</strong> Доступен в личном кабинете PayOnline System';
$MESS['PAYONLINE_ACTIVE_TITLE'] = 'Активность';
$MESS['PAYONLINE_SITE_TITLE'] = 'Сайт';
$MESS['PAYONLINE_PAY_SYSTEM_TITLE'] = 'Название';
$MESS['PAYONLINE_PAY_SYSTEM_DESCRIPTION'] = 'Описание';
$MESS['PAYONLINE_PAYER_TYPES_TITLE'] = 'Типы плательщиков';

$MESS['PAYONLINE_INSTALLATION_NOTE'] = 'Помимо вышеуказанных настроек при создании новой платежной системы будут заданы следующие параметры обработчика:
<ul>
	<li><b>Адрес формы оплаты или выбора платежного инструмента:</b> https://secure.payonlinesystem.com/ru/payment/select/;</li>
	<li><b>Адрес обратной связи:</b> http://домен_сайта/bitrix/admin/payonline_callback.php;</li>
	<li><b>Абсолютный URL адрес, на который будет отправлен плательщик после завершения платежа:</b> http://домен_сайта/payonline/;</li>
	<li><b>Максимальный срок платежа с момента оформления заказа:</b> 720 часов (30 дней);</li>
	<li><b>Производить отмену транзакции при отмене заказа:</b> да;</li>
	<li><b>Производить возврат денег по успешной транзакции при отмене заказа:</b> да.</li>
</ul>
Все параметры будут доступны для изменения на странице редактирования созданной платежной системы после установки модуля.';