<?

$MESS['PAYONLINE_TITLE'] = 'Оплата через PayOnline System';
$MESS['PAYONLINE_DESCRIPTION'] = 'Оплата через PayOnline System';

$MESS['PAYONLINE_MERCHANT_ID_NAME'] = 'Номер учетной записи';
$MESS['PAYONLINE_MERCHANT_ID_DESCR'] = '<strong>Merchant ID.</strong> Доступен в личном кабинете PayOnline System';
$MESS['PAYONLINE_SECURITY_KEY_NAME'] = 'Секретный ключ';
$MESS['PAYONLINE_SECURITY_KEY_DESCR'] = '<strong>Security key.</strong> Доступен в личном кабинете PayOnline System';
$MESS['PAYONLINE_PAYMENT_URL_NAME'] = 'Адрес формы оплаты или выбора платежного инструмента';
$MESS['PAYONLINE_PAYMENT_URL_DESCR'] = 'Возможные значения:<br /><strong>https://secure.payonlinesystem.com/ru/payment/select/</strong> - форма выбора платежного инструмента;<br /><strong>https://secure.payonlinesystem.com/ru/payment/select/qiwi/</strong> - форма оплаты через QIWI;<br /><strong>https://secure.payonlinesystem.com/ru/payment/select/webmoney/</strong> - форма оплаты через WebMoney;<br /><strong>https://secure.payonlinesystem.com/ru/payment/</strong> - форма оплаты с банковской карты.';
$MESS['PAYONLINE_CALL_BACK_URL_NAME'] = 'Адрес обратной связи';
$MESS['PAYONLINE_CALL_BACK_URL_DESCR'] = 'Ресурс на Вашем сайте, который будет автоматически вызван системой в случае успешного прохождения платежа. Адрес может включать параметры, например <a href="http://hostname/confirm.php?from=PayOnlineSystem">http://hostname/confirm.php?from=PayOnlineSystem</a>. Настройка адреса допускается только на 80 порт по протоколу HTTP либо HTTPS. Для работы HTTPS протокола на стороне ТСП должен быть валидный SSL сертификат.';
$MESS['PAYONLINE_RETURN_URL_NAME'] = 'Абсолютный URL адрес, на который будет отправлен плательщик после завершения платежа';
$MESS['PAYONLINE_ORDER_ID_NAME'] = 'Номер заказа';
$MESS['PAYONLINE_SHOULD_PAY_NAME'] = 'Сумма к оплате';
$MESS['PAYONLINE_CURRENCY_NAME'] = 'Валюта заказа';
$MESS['PAYONLINE_LOCATION_COUNTRY_NAME'] = 'Страна';
$MESS['PAYONLINE_LOCATION_CITY_NAME'] = 'Город';
$MESS['PAYONLINE_ADDRESS_NAME'] = 'Адрес (без города)';
$MESS['PAYONLINE_INDEX_NAME'] = 'Индекс';
$MESS['PAYONLINE_PHONE_NAME'] = 'Номер телефона';
$MESS['PAYONLINE_EMAIL_NAME'] = 'E-mail';
$MESS['PAYONLINE_VALID_UNTIL_NAME'] = 'Срок платежа';
$MESS['PAYONLINE_VALID_UNTIL_DESCR'] = 'Максимальный срок платежа с момента оформления заказа (в часах).<br />Если оставить пустым, срок устанавливаться не будет';
$MESS['PAYONLINE_CAN_VOID_NAME'] = 'Производить отмену транзакции при отмене заказа?';
$MESS['PAYONLINE_CAN_VOID_DESCR'] = 'Возможные значения:<br /><strong>Y</strong> - да;<br /><strong>N</strong> - нет.<br />Отмена транзакции возможна в течение 24 часов с момента прохождения платежа.';
$MESS['PAYONLINE_CAN_REFUND_NAME'] = 'Производить возврат денег по успешной транзакции при отмене заказа?';
$MESS['PAYONLINE_CAN_REFUND_DESCR'] = 'Возможные значения:<br /><strong>Y</strong> - да;<br /><strong>N</strong> - нет.<br />Возврат денег возможен через 24 часа с момента прохождения платежа и до 180 суток.';