<?

class CPayOnline
{
	function OnSaleCancelOrder($ID, $val)
	{
		if ( ($arOrder = CSaleOrder::GetByID($ID)) && $val == 'Y' && strpos($arOrder['PS_STATUS_DESCRIPTION'], 'PayOnline:') === 0)
		{
			$arStatus = explode(':', $arOrder['PS_STATUS_DESCRIPTION']);

			if ( count($arStatus) == 3 && $arStatus[1] == 'Card' )
			{
				CSalePaySystemAction::InitParamArrays($arOrder, $arOrder['ID']);
				$CanVoid = CSalePaySystemAction::GetParamValue('CAN_VOID');
				$CanRefund = CSalePaySystemAction::GetParamValue('CAN_REFUND');

				if ( $CanVoid == 'Y' )
				{
					$MerchantId = CSalePaySystemAction::GetParamValue('MERCHANT_ID');
					$PrivateSecurityKey = CSalePaySystemAction::GetParamValue('PRIVATE_SECURITY_KEY');
					$TransactionId = $arStatus[2];

					$params_string = 'MerchantId='. $MerchantId;
					$params_string .= '&TransactionId='. $TransactionId;
					$params_string .= '&PrivateSecurityKey='. $PrivateSecurityKey;

					$SecurityKey = md5($params_string);

					$sHost = 'secure.payonlinesystem.com';
					$sUrl = '/payment/transaction/void/';

					$sVars = 'MerchantId='. urlencode($MerchantId);
					$sVars .= '&TransactionId='. urlencode($TransactionId);
					$sVars .= '&SecurityKey='. urlencode($SecurityKey);
					$sVars .= '&ContentType=xml';

					$sResult = QueryGetData($sHost, 443, $sUrl, $sVars, $errno, $errstr, 'GET', 'ssl://');

					if ( $sResult && !intval($errno) )
					{
						require_once($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/main/classes/general/xml.php');

						$objXML = new CDataXML();
						$objXML->LoadString($sResult);
						$arResult = $objXML->GetArray();

						if ( $arResult['transaction']['#']['result']['0']['#'] == 'Error' && $CanRefund == 'Y' )
						{
							$Amount = $arOrder['PS_SUM'];

							$params_string = 'MerchantId='. $MerchantId;
							$params_string .= '&TransactionId='. $TransactionId;
							$params_string .= '&Amount='. $Amount;
							$params_string .= '&PrivateSecurityKey='. $PrivateSecurityKey;

							$SecurityKey = md5($params_string);

							$sUrl = '/payment/transaction/refund/';

							$sVars = 'MerchantId='. urlencode($MerchantId);
							$sVars .= '&TransactionId='. urlencode($TransactionId);
							$sVars .= '&Amount='. urlencode($Amount);
							$sVars .= '&SecurityKey='. urlencode($SecurityKey);
							$sVars .= '&ContentType=xml';

							$sResult = QueryGetData($sHost, 443, $sUrl, $sVars, $errno, $errstr, 'GET', 'ssl://');
						}
					}
				}
			}
		}
	}
}