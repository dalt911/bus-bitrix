<?

if ( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true )
{
	die();
}

global $MESS;

$strPath2Lang = str_replace('\\', '/', __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen('/payment/payonline/description.php'));
include(GetLangFileName($strPath2Lang .'/lang/', '/payment/payonline/description.php'));

$psTitle = GetMessage('PAYONLINE_TITLE');
$psDescription = GetMessage('PAYONLINE_DESCRIPTION');

$arPSCorrespondence = array(
	'MERCHANT_ID' => array(
		'NAME' => GetMessage('PAYONLINE_MERCHANT_ID_NAME'),
		'DESCR' => GetMessage('PAYONLINE_MERCHANT_ID_DESCR'),
		'VALUE' => '',
		'TYPE' => ''
	),
	'PRIVATE_SECURITY_KEY' => array(
		'NAME' => GetMessage('PAYONLINE_SECURITY_KEY_NAME'),
		'DESCR' => GetMessage('PAYONLINE_SECURITY_KEY_DESCR'),
		'VALUE' => '',
		'TYPE' => ''
	),
	'PAYMENT_URL' => array(
		'NAME' => GetMessage('PAYONLINE_PAYMENT_URL_NAME'),
		'DESCR' => GetMessage('PAYONLINE_PAYMENT_URL_DESCR'),
		'VALUE' => 'https://secure.payonlinesystem.com/ru/payment/select/',
		'TYPE' => ''
	),
	'CALL_BACK_URL' => array(
		'NAME' => GetMessage('PAYONLINE_CALL_BACK_URL_NAME'),
		'DESCR' => GetMessage('PAYONLINE_CALL_BACK_URL_DESCR'),
		'VALUE' => '',
		'TYPE' => ''
	),
	'RETURN_URL' => array(
		'NAME' => GetMessage('PAYONLINE_RETURN_URL_NAME'),
		'VALUE' => '',
		'TYPE' => ''
	),
	'ORDER_ID' => array(
		'NAME' => GetMessage('PAYONLINE_ORDER_ID_NAME'),
		'VALUE' => 'ID',
		'TYPE' => 'ORDER'
	),
	'SHOULD_PAY' => array(
		'NAME' => GetMessage('PAYONLINE_SHOULD_PAY_NAME'),
		'VALUE' => 'SHOULD_PAY',
		'TYPE' => 'ORDER'
	),
	'CURRENCY' => array(
		'NAME' => GetMessage('PAYONLINE_CURRENCY_NAME'),
		'VALUE' => 'CURRENCY',
		'TYPE' => 'ORDER'
	),
	'LOCATION_COUNTRY' => array(
		'NAME' => GetMessage('PAYONLINE_LOCATION_COUNTRY_NAME'),
		'VALUE' => 'LOCATION_COUNTRY',
		'TYPE' => 'PROPERTY'
	),
	'LOCATION_CITY' => array(
		'NAME' => GetMessage('PAYONLINE_LOCATION_CITY_NAME'),
		'VALUE' => 'LOCATION_CITY',
		'TYPE' => 'PROPERTY'
	),
	'ADDRESS' => array(
		'NAME' => GetMessage('PAYONLINE_ADDRESS_NAME'),
		'VALUE' => 'ADDRESS',
		'TYPE' => 'PROPERTY'
	),
	'INDEX' => array(
		'NAME' => GetMessage('PAYONLINE_INDEX_NAME'),
		'VALUE' => 'INDEX',
		'TYPE' => 'PROPERTY'
	),
	'PHONE' => array(
		'NAME' => GetMessage('PAYONLINE_PHONE_NAME'),
		'VALUE' => 'PERSONAL_MOBILE',
		'TYPE' => 'USER'
	),
	'EMAIL' => array(
		'NAME' => GetMessage('PAYONLINE_EMAIL_NAME'),
		'VALUE' => 'EMAIL',
		'TYPE' => 'PROPERTY'
	),
	'VALID_UNTIL' => array(
		'NAME' => GetMessage('PAYONLINE_VALID_UNTIL_NAME'),
		'DESCR' => GetMessage('PAYONLINE_VALID_UNTIL_DESCR'),
		'VALUE' => '720',
		'TYPE' => ''
	),
	'CAN_VOID' => array(
		'NAME' => GetMessage('PAYONLINE_CAN_VOID_NAME'),
		'DESCR' => GetMessage('PAYONLINE_CAN_VOID_DESCR'),
		'VALUE' => 'Y',
		'TYPE' => ''
	),
	'CAN_REFUND' => array(
		'NAME' => GetMessage('PAYONLINE_CAN_REFUND_NAME'),
		'DESCR' => GetMessage('PAYONLINE_CAN_REFUND_DESCR'),
		'VALUE' => 'Y',
		'TYPE' => ''
	)
);