<?

if ( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true ) 
{
	die();
}

if ( CModule::IncludeModule('sale') )
{
	if ( isset($_POST['OrderId']) )
	{
		$params = $_POST;
	}
	elseif ( isset($_GET['OrderId']) )
	{
		$params = $_GET;
	}
	else
	{
		$params = false;
	}
	
	if ( $params && ($arOrder = CSaleOrder::GetByID(intval($params['OrderId']))) )
	{
		CSalePaySystemAction::InitParamArrays($arOrder, $arOrder['ID']);
		$PrivateSecurityKey = CSalePaySystemAction::GetParamValue('PRIVATE_SECURITY_KEY');
	
		if ( isset($params['SecurityKey'], $params['TransactionID'], $params['OrderId'], $params['Amount'], $params['Currency'], $params['DateTime']) )
		{
			$params_string = 'DateTime='. $params['DateTime'];
			$params_string .= '&TransactionID='. $params['TransactionID'];
			$params_string .= '&OrderId='. $params['OrderId'];
			$params_string .= '&Amount='. $params['Amount'];
			$params_string .= '&Currency='. $params['Currency'];
			$params_string .= '&PrivateSecurityKey='. $PrivateSecurityKey;
		
			if ( (md5($params_string) == $params['SecurityKey']) )
			{
				$Provider = $params['Provider'];
				
				$strPS_STATUS_MESSAGE = 'дата платежа - '. $params['DateTime'] .'; ';
				$strPS_STATUS_MESSAGE .= 'идентификатор транзакции - '. $params['TransactionID'] .'; ';
				$strPS_STATUS_MESSAGE .= 'идентификатор заказа - '. $params['OrderId'] .'; ';
				
				$PayOrder = false;
				$arFields = array(
					'PS_STATUS' => 'Y',
					'PS_STATUS_CODE' => 'OK',
					'PS_STATUS_DESCRIPTION' => 'PayOnline:'. $Provider .':'. $params['TransactionID'],
					'PS_STATUS_MESSAGE' => '',
					'PS_SUM' => $params['Amount'],
					'PS_CURRENCY' => $params['Currency'],
					'PS_RESPONSE_DATE' => Date(CDatabase::DateFormatToPHP(CLang::GetDateFormat('FULL', LANG))),
					'USER_ID' => $arOrder['USER_ID']
				);
				
				switch ( $Provider )
				{
					case 'Card':
						if ( isset($params['CardHolder'], $params['CardNumber'], $params['Country'], $params['City']) )
						{
							$CardHolder = $params['CardHolder'];
							$CardNumber = $params['CardNumber'];
							$Country = $params['Country'];
							$City = $params['City'];
							$Address = ( (isset($params['Address']) && $params['Address']) ? $params['Address'] : false );
							
							$PayOrder = true;
							$strPS_STATUS_MESSAGE .= 'имя держателя карты - '. $CardHolder. '; ';
							$strPS_STATUS_MESSAGE .= 'маскированный номер карты - '. $CardNumber .'; ';
							$strPS_STATUS_MESSAGE .= 'код страны - '. $Country .'; ';
							$strPS_STATUS_MESSAGE .= 'город - '. $City;
							if ( $Address )
							{
								$strPS_STATUS_MESSAGE .= '; улица - '. $Address;
							}
						}
						break;
					
					case 'Qiwi':
						if ( isset($params['Phone']) )
						{
							$Phone = $params['Phone'];
							
							$PayOrder = true;
							$strPS_STATUS_MESSAGE .= 'номер телефона - '. $Phone;
						}
						break;
					
					case 'WebMoney':
						if ( isset($params['WmTranId'], $params['WmInvId']) )
						{
							$WmTranId = $params['WmTranId'];
							$WmInvId = $params['WmInvId'];
							$WmId = ( (isset($params['WmId']) && $params['WmId']) ? $params['WmId'] : false );
							$WmPurse = ( (isset($params['WmPurse']) && $params['WmPurse']) ? $params['WmPurse'] : false );
							
							$PayOrder = true;
							$strPS_STATUS_MESSAGE .= 'служебный номер счета в системе учета WebMoney - '. $WmTranId .'; ';
							$strPS_STATUS_MESSAGE .= 'уникальный номер счета в системе учета WebMoney - '. $WmInvId;
							if ( $WmId )
							{
								$strPS_STATUS_MESSAGE .= '; WMID плательщика - '. $WmId;
							}
							if ( $WmPurse )
							{
								$strPS_STATUS_MESSAGE .= '; WM кошелек плательщика - '. $WmPurse;
							}
						}
						break;
				}
				
				if ( $PayOrder )
				{
					$arFields['PS_STATUS_MESSAGE'] = $strPS_STATUS_MESSAGE;
					CSaleOrder::PayOrder($arOrder['ID'], 'Y');
					CSaleOrder::Update($arOrder['ID'], $arFields);
				}
			}
		}
	}
}