<?

if ( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true ) 
{
	die();
}

$strPath = str_replace('\\', '/', __FILE__);
$strPath = substr($strPath, 0, strlen($strPath) - strlen('payment.php'));
include($strPath .'codes.php');

$PrivateSecurityKey = CSalePaySystemAction::GetParamValue('PRIVATE_SECURITY_KEY');
$MerchantId = CSalePaySystemAction::GetParamValue('MERCHANT_ID');
$OrderId = CSalePaySystemAction::GetParamValue('ORDER_ID');
$Amount = number_format(CSalePaySystemAction::GetParamValue('SHOULD_PAY'), 2, '.', '');
$Currency = CSalePaySystemAction::GetParamValue('CURRENCY');

$Encoding = ( defined('BS_SALE_ENCODING') ? BX_SALE_ENCODING : ( defined('SITE_CHARSET') ? SITE_CHARSET : false ) );

$Country = CSalePaySystemAction::GetParamValue('LOCATION_COUNTRY');
if ( !in_array(strtoupper($Country), $COUNTRY_CODES) )
{
	$LocationCountryEn = false;
	$dbOrderPropValues = CSaleOrderPropsValue::GetList(
		array(),
		array('ORDER_ID' => CSalePaySystemAction::GetParamValue('ORDER_ID')),
		false,
		false,
		array('ID', 'CODE', 'VALUE', 'ORDER_PROPS_ID', 'PROP_TYPE')
	);
	while ($arOrderPropValues = $dbOrderPropValues->Fetch())
	{
		$arOrderProps = CSaleOrderProps::GetRealValue(
			$arOrderPropValues['ORDER_PROPS_ID'],
			$arOrderPropValues['CODE'],
			$arOrderPropValues['PROP_TYPE'],
			$arOrderPropValues['VALUE'],
			'en'
		);
		if ( is_array($arOrderProps) && isset($arOrderProps['LOCATION_COUNTRY']) )
		{
			$LocationCountryEn = $arOrderProps['LOCATION_COUNTRY'];
		}
	}
	if ( $LocationCountryEn && isset($COUNTRY_CODES[strtoupper($LocationCountryEn)]) )
	{
		$Country = $COUNTRY_CODES[strtoupper($LocationCountryEn)];
	}
	else
	{
		$Country = false;
	}
}

$City = CSalePaySystemAction::GetParamValue('LOCATION_CITY');
$Address = CSalePaySystemAction::GetParamValue('ADDRESS');
$Zip = CSalePaySystemAction::GetParamValue('INDEX');
$Phone = CSalePaySystemAction::GetParamValue('PHONE');
$Email = CSalePaySystemAction::GetParamValue('EMAIL');

$ReturnUrl = CSalePaySystemAction::GetParamValue('RETURN_URL');

if ( ($arOrder = CSaleOrder::GetByID($OrderId)) && ($ValidUntil = CSalePaySystemAction::GetParamValue('VALID_UNTIL')) )
{
	$ValidUntil = strftime('%Y-%m-%d %T', MakeTimeStamp($arOrder['DATE_INSERT'], 'YYYY-MM-DD HH:MI:SS') - date('O')/100*60*60 + $ValidUntil*60*60);
}

$params = 'MerchantId='. $MerchantId;
$params .= '&OrderId='. $OrderId;
$params .= '&Amount='. $Amount;
$params .= '&Currency='. $Currency;
if ( $ValidUntil )
{
	$params .= '&ValidUntil='. $ValidUntil;
}
$params .= '&PrivateSecurityKey='. $PrivateSecurityKey;

$SecurityKey = md5($params);

?>
<form name="ShopForm" action="<?=CSalePaySystemAction::GetParamValue('PAYMENT_URL')?>" method="post">
	<input type="hidden" name="MerchantId" value="<?=$MerchantId?>" />
	<input type="hidden" name="OrderId" value="<?=$OrderId?>" />
	<input type="hidden" name="Amount" value="<?=$Amount?>" />
	<input type="hidden" name="Currency" value="<?=$Currency?>" />
	<input type="hidden" name="SecurityKey" value="<?=$SecurityKey?>" />

	<?if($Country):?><input type="hidden" name="Country" value="<?=strtoupper($Country)?>" /><?endif?>
	<?if($City):?><input type="hidden" name="City" value="<?=$City?>" /><?endif?>
	<?if($Address):?><input type="hidden" name="Address" value="<?=$Address?>" /><?endif?>
	<?if($Zip):?><input type="hidden" name="Zip" value="<?=$Zip?>" /><?endif?>
	<?if($Phone):?><input type="hidden" name="Phone" value="<?=$Phone?>" /><?endif?>
	<?if($Email):?><input type="hidden" name="Email" value="<?=$Email?>" /><?endif?>
	<?if($Encoding):?><input type="hidden" name="Encoding" value="<?=strtolower($Encoding)?>" /><?endif?>

	<?if($ReturnUrl):?><input type="hidden" name="ReturnUrl" value="<?=$ReturnUrl?>" /><?endif?>
	<?if($ValidUntil):?><input type="hidden" name="ValidUntil" value="<?=$ValidUntil?>" /><?endif?>
	<input type="submit" value="Оплатить" />
</form>