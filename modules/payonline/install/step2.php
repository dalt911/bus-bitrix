<?

global $MESS;

$strPath2Lang = str_replace('\\', '/', __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen('/install/step2.php'));
include(GetLangFileName($strPath2Lang .'/lang/', '/install/step2.php'));

global $errors;

if ( !check_bitrix_sessid() )
{
	return;
}

if ( CModule::IncludeModule('sale') )
{
	$dbSite = CSite::GetByID($pay_system_site);
	$arSite = $dbSite->Fetch();

	if ( $arSite )
	{
		$domain = false;

		if ( $arSite['DOMAINS'] )
		{
			$domain_list = explode("\n", $arSite['DOMAINS']);
			if ( !empty($domain_list) )
			{
				$domain = $domain_list[0];
			}
		}

		if ( !$domain )
		{
			$domain = $_SERVER['HTTP_HOST'];
		}

		$pay_system_id = CSalePaySystem::Add(array(
			'LID' => $arSite['ID'],
			'CURRENCY' => 'RUB',
			'NAME' => $pay_system_name,
			'ACTIVE' => ( ($activate_pay_system == 'Y') ? 'Y' : 'N' ),
			'DESCRIPTION' => $pay_system_description
		));

		if ( $pay_system_id && is_array($sale_person_type_list) )
		{
			$params = array(
				'MERCHANT_ID' => array(
					'TYPE' => '',
					'VALUE' => $payonline_merchant_id
				),
				'PRIVATE_SECURITY_KEY' => array(
					'TYPE' => '',
					'VALUE' => $payonline_security_key
				),
				'PAYMENT_URL' => array(
					'TYPE' => '',
					'VALUE' => 'https://secure.payonlinesystem.com/ru/payment/select/'
				),
				'CALL_BACK_URL' => array(
					'TYPE' => '',
					'VALUE' => 'http://'. $domain .'/bitrix/admin/payonline_callback.php'
				),
				'RETURN_URL' => array(
					'TYPE' => '',
					'VALUE' => 'http://'. $domain .'/payonline/'
				),
				'ORDER_ID' => array(
					'TYPE' => 'ORDER',
					'VALUE' => 'ID'
				),
				'SHOULD_PAY' => array(
					'TYPE' => 'ORDER',
					'VALUE' => 'SHOULD_PAY'
				),
				'CURRENCY' => array(
					'TYPE' => 'ORDER',
					'VALUE' => 'CURRENCY'
				),
				'LOCATION_COUNTRY' => array(
					'VALUE' => 'LOCATION_COUNTRY',
					'TYPE' => 'PROPERTY'
				),
				'LOCATION_CITY' => array(
					'VALUE' => 'LOCATION_CITY',
					'TYPE' => 'PROPERTY'
				),
				'ADDRESS' => array(
					'VALUE' => 'ADDRESS',
					'TYPE' => 'PROPERTY'
				),
				'INDEX' => array(
					'VALUE' => 'INDEX',
					'TYPE' => 'PROPERTY'
				),
				'PHONE' => array(
					'VALUE' => 'PERSONAL_MOBILE',
					'TYPE' => 'USER'
				),
				'EMAIL' => array(
					'VALUE' => 'EMAIL',
					'TYPE' => 'PROPERTY'
				),
				'VALID_UNTIL' => array(
					'TYPE' => '',
					'VALUE' => '720',
				),
				'CAN_VOID' => array(
					'TYPE' => '',
					'VALUE' => 'Y'
				),
				'CAN_REFUND' => array(
					'TYPE' => '',
					'VALUE' => 'Y'
				)
			);

			foreach ( $sale_person_type_list as $sale_person_type_id )
			{
				if ( $arSalePersonType = CSalePersonType::GetByID(intval($sale_person_type_id)) )
				{
					$pay_system_action_id = CSalePaySystemAction::Add(array(
						'PAY_SYSTEM_ID' => $pay_system_id,
						'PERSON_TYPE_ID' => $arSalePersonType['ID'],
						'NAME' => $pay_system_name,
						'ACTION_FILE' => '/bitrix/php_interface/include/sale_payment/payonline',
						'NEW_WINDOW' => 'N',
						'PARAMS' => serialize($params),
						'HAVE_PAYMENT' => 'Y'
					));
				}
			}
		}

		CopyDirFiles($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/payonline/install/public', $arSite['ABS_DOC_ROOT'] . $arSite['DIR'] .'payonline', false, true);
	}
}

if ( strlen($errors) <= 0 )
{
	echo CAdminMessage::ShowNote(GetMessage('PAYONLINE_INSTALLATION_SUCCESSFULL'));
	echo BeginNote();
	echo GetMessage('PAYONLINE_INSTALLATION_SUCCESSFULL_NOTE');
	echo EndNote();
}
else
{
	echo CAdminMessage::ShowMessage(
		array(
			'TYPE' => 'ERROR',
			'MESSAGE' => GetMessage('PAYONLINE_INSTALLATION_ERROR'),
			'DETAILS' => $errors,
			'HTML' => true
		)
	);
}

?>
<form action="<?=$APPLICATION->GetCurPage()?>">
	<input type="hidden" name="lang" value="<?=LANG?>" />
	<input type="submit" name="" value="<?=GetMessage('PAYONLINE_RETURN_BACK')?>" />
</form>