<?

global $MESS;

$strPath2Lang = str_replace('\\', '/', __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen('/install/index.php'));
include(GetLangFileName($strPath2Lang .'/lang/', '/install/index.php'));

if ( class_exists('payonline') )
{
	return;
}

class payonline extends CModule
{
	var $MODULE_ID = 'payonline';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = 'Y';

	function payonline()
	{
		$arModuleVersion = array();

		$path = str_replace('\\', '/', __FILE__);
		$path = substr($path, 0, strlen($path) - strlen('/index.php'));

		include($path .'/version.php');

		if ( is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion) )
		{
			$this->MODULE_VERSION = $arModuleVersion['VERSION'];
			$this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
		}
		else
		{
			$this->MODULE_VERSION = PAYONLINE_VERSION;
			$this->MODULE_VERSION_DATE = PAYONLINE_VERSION_DATE;
		}

		$this->MODULE_NAME = GetMessage('PAYONLINE_INSTALL_NAME');
		$this->MODULE_DESCRIPTION = GetMessage('PAYONLINE_INSTALL_DESCRIPTION');
	}

	function DoInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION, $step;

		$step = intval($step);

		if ( $step < 2 )
		{
			$APPLICATION->IncludeAdminFile(GetMessage('PAYONLINE_INSTALL_TITLE'), $DOCUMENT_ROOT .'/bitrix/modules/payonline/install/step1.php');
		}
		elseif ( $step == 2 )
		{
			$this->InstallFiles();
			$this->InstallDB();
			$GLOBALS['errors'] = $this->errors;

			$APPLICATION->IncludeAdminFile(GetMessage('PAYONLINE_INSTALL_TITLE'), $DOCUMENT_ROOT .'/bitrix/modules/payonline/install/step2.php');
		}
	}

	function InstallFiles()
	{
		global $DOCUMENT_ROOT;

		CopyDirFiles($DOCUMENT_ROOT .'/bitrix/modules/payonline/install/admin', $DOCUMENT_ROOT .'/bitrix/admin');

		$ToDir = $DOCUMENT_ROOT .'/bitrix/php_interface/include/sale_payment/payonline';
		CheckDirPath($ToDir);
		CopyDirFiles($DOCUMENT_ROOT .'/bitrix/modules/payonline/install/php_interface/include/sale_payment/payonline', $ToDir);

		return true;
	}

	function InstallDB()
	{
		RegisterModule('payonline');
		RegisterModuleDependences('sale', 'OnSaleCancelOrder', 'payonline', 'CPayOnline', 'OnSaleCancelOrder');

		return true;
	}

	function DoUnInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION, $step, $errors;

		$step = intval($step);

		if ( $step < 2 )
		{
			$APPLICATION->IncludeAdminFile(GetMessage('PAYONLINE_UNINSTALL_TITLE'), $DOCUMENT_ROOT .'/bitrix/modules/payonline/install/unstep1.php');
		}
		elseif ( $step == 2 )
		{
			$errors = false;

			$this->UnInstallDB();
			$this->UnInstallFiles();

			$APPLICATION->IncludeAdminFile(GetMessage('PAYONLINE_UNINSTALL_TITLE'), $DOCUMENT_ROOT .'/bitrix/modules/payonline/install/unstep2.php');
		}
	}

	function UnInstallFiles()
	{
		global $DOCUMENT_ROOT;

		DeleteDirFiles($DOCUMENT_ROOT .'/bitrix/modules/payonline/install/admin', $DOCUMENT_ROOT .'/bitrix/admin');
		DeleteDirFilesEx('/bitrix/php_interface/include/sale_payment/payonline');

		return true;
	}

	function UnInstallDB()
	{
		UnRegisterModuleDependences('sale', 'OnSaleCancelOrder', 'payonline', 'CPayOnline', 'OnSaleCancelOrder');
		UnRegisterModule('payonline');

		return true;
	}
}