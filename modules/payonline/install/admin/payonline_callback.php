<?

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);

require($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/main/include/prolog_before.php');

if ( file_exists($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/payonline/payment/payonline/callback.php') )
{
	include($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/payonline/payment/payonline/callback.php');
}

require($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/main/include/epilog_after.php');