<?

global $MESS;

$strPath2Lang = str_replace('\\', '/', __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - strlen('/install/step1.php'));
include(GetLangFileName($strPath2Lang .'/lang/', '/install/step1.php'));

if ( CModule::IncludeModule('sale') )
{
	$dbSiteList = CSite::GetList($by='sort', $order='asc', array('ACTIVE' => 'Y') );
	$arSiteSelect = array();
	while ( $arSite = $dbSiteList->GetNext() )
	{
		$arSiteSelect[$arSite['ID']] = $arSite['NAME'];
	}

	$dbSalePersonTypeList = CSalePersonType::GetList(array('SORT' => 'ASC'));
	$arSalePersonTypeList = array();
	while ( $arSalePersonType = $dbSalePersonTypeList->GetNext() )
	{
		$arSalePersonTypeList[$arSalePersonType['ID']] = $arSalePersonType['NAME'];
	}
}

$arTabs = array(
	array('DIV' => 'edit1', 'TAB' => GetMessage('PAYONLINE_SETTINGS_TAB_NAME'), 'TITLE' => GetMessage('PAYONLINE_SETTINGS_TAB_TITLE')),
	array('DIV' => 'edit2', 'TAB' => GetMessage('PAYONLINE_PAY_SYSTEM_TAB_NAME'), 'TITLE' => GetMessage('PAYONLINE_PAY_SYSTEM_TAB_TITLE'))
);

$tabControl = new CAdminTabControl('tabControl', $arTabs);

?>
<form action="<?=$APPLICATION->GetCurPage()?>">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="lang" value="<?= LANG ?>" />
	<input type="hidden" name="id" value="payonline" />
	<input type="hidden" name="install" value="Y" />
	<input type="hidden" name="step" value="2" />
	<?$tabControl->Begin();?>
	<?$tabControl->BeginNextTab();?>
		<tr valign="top">
			<td width="40%"><span class="required">*</span><label for="payonline_security_key"><?=GetMessage('PAYONLINE_MERCHANT_ID_TITLE')?></label>:<br /><small>(<?=GetMessage('PAYONLINE_MERCHANT_ID_DESCRIPTION')?>)</small></td>
			<td width="60%"><input type="text" name="payonline_merchant_id" value="" id="payonline_merchant_id" size="40" /></td>
		</tr>
		<tr valign="top">
			<td><span class="required">*</span><label for="payonline_security_key">Секретный ключ</label>:<br /><small>(<strong>Security key.</strong> Доступен в личном кабинете PayOnline System)</small></td>
			<td><input type="text" name="payonline_security_key" value="" id="payonline_security_key" size="40" /></td>
		</tr>
	<?$tabControl->BeginNextTab();?>
		<tr valign="top">
			<td width="40%"><label for="activate_pay_system"><?=GetMessage('PAYONLINE_ACTIVE_TITLE')?></label>:</td>
			<td width="60%"><input type="checkbox" name="activate_pay_system" value="Y" id="activate_pay_system" checked="checked" /></td>
		</tr>
		<tr valign="top">
			<td><span class="required">*</span><label for="pay_system_site"><?=GetMessage('PAYONLINE_SITE_TITLE')?></label>:</td>
			<td>
				<select name="pay_system_site" id="pay_system_site" />
					<?foreach($arSiteSelect as $value=>$name):?>
						<option value="<?=$value?>"><?=$name?></option>
					<?endforeach?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<td><span class="required">*</span><label for="pay_system_title"><?=GetMessage('PAYONLINE_PAY_SYSTEM_TITLE')?></label>:</td>
			<td><input type="text" name="pay_system_name" value="Оплата в платежной системе PayOnline System" size="40" /></td>
		</tr>
		<tr valign="top">
			<td><span class="required">*</span><label for="pay_system_description"><?=GetMessage('PAYONLINE_PAY_SYSTEM_DESCRIPTION')?></label>:</td>
			<td><textarea name="pay_system_description" id="" cols="30" rows="10">Оплата с помощью кредитной карты, WebMoney, QIWI</textarea></td>
		</tr>
		<tr valign="top">
			<td><span class="required">*</span><?=GetMessage('PAYONLINE_PAYER_TYPES_TITLE')?>:</td>
			<td>
				<?foreach($arSalePersonTypeList as $value=>$name):?>
					<label><input type="checkbox" name="sale_person_type_list[]" value="<?=$value?>" checked="checked" /> <?=$name?></label><br />
				<?endforeach?>
			</td>
		</tr>
	<?$tabControl->Buttons();?>
		<input type="submit" name="" value="<?=GetMessage('MOD_INSTALL')?>" />
	<?$tabControl->End();?>
</form>
<?echo BeginNote();?>
<span class="required">*</span> - <?echo GetMessage('REQUIRED_FIELDS')?><br /><br />
<?=GetMessage('PAYONLINE_INSTALLATION_NOTE')?>
<?echo EndNote();?>