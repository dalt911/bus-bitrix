<?
require($_SERVER['DOCUMENT_ROOT'] .'/bitrix/header.php');
$APPLICATION->SetPageProperty('title', 'Оплата заказа прошла успешно');
$APPLICATION->SetTitle('Оплата заказа прошла успешно');
?>
<p>Оплата Вашего заказа успешно завершена.</p>
<p>Вы можете отслеживать дальнейшее состояние своих заказов <a href="/personal/order/">в своем профиле</a>.</p>
<?require($_SERVER['DOCUMENT_ROOT'] .'/bitrix/footer.php');?>