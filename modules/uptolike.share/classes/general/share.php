<?
/** var CMain $APPLICATION */
IncludeModuleLangFile(__FILE__);

Class CUptolikeShare 
{
	
	const HOST = 'https://uptolike.com/';
	const PARTNER_ID = "cms";
	
	function ini()
    {
		global $APPLICATION;
		$dir = $APPLICATION->GetCurDir();
		$dirs = explode('/', $dir);
		if($dirs[1] == 'bitrix')
		{
			CJSCore::Init(array("jquery"));
		}
    }
	
	function projectId()
	{
		$site_url = preg_replace('/^www\./', '', $_SERVER["HTTP_HOST"]);
		$site_url = str_replace(array('.', '-', '_'), '', $site_url);
		return self::PARTNER_ID.$site_url;
	}
	
	function userReg($email)
	{
	
		if ($email !== '' && self::PARTNER_ID !== '' && self::projectId() !== '')
		{
			$url = self::HOST.'api/getCryptKeyWithUserReg.json?'.http_build_query(array(
					'email' => $email,
					'partner' => self::PARTNER_ID,
					'projectId' => self::projectId()));
	
			$jsonAnswer = file_get_contents($url);
			if (false !== $jsonAnswer)
			{
				$answer = json_decode($jsonAnswer);
				if($answer->errorMessage == null and ($answer->statusCode == "MAIL_SENDED" || $answer->statusCode == "ALREADY_EXISTS"))
				{
					return $answer->statusCode;
				}
				elseif($answer->errorMessage == null and $answer->statusCode == "SUCCESS")
				{
					return $answer->cryptKey; 
				}
				else
				{
					return "error";
				}
			}
			else
			{
				return $jsonAnswer;
			}
	
		}
		else
		{
			return 'one of params is empty';
		}
	}	
	
	function statIframe($mail = '', $cryptKey = '')
	{
		$params = array(
			'mail' => $mail,
			'partner' => self::PARTNER_ID,
			'projectId' => self::projectId(),	
		);
		$paramsStr = 'mail='.$mail.'&partner='.self::PARTNER_ID.'&projectId='.self::projectId();
		$signature = md5($paramsStr . $cryptKey);
		$params['signature'] = $signature;
		return self::HOST.'api/statistics.html?' . http_build_query($params);
	}	
	
	function constructorIframe($mail = '', $cryptKey = '')
	{		
		$params = array (
			'mail' => $mail,
			'partner' => self::PARTNER_ID,
			'projectId' => self::projectId()
		);			
		$paramsStr = 'mail='.$mail.'&partner='.self::PARTNER_ID.'&projectId='.self::projectId().$cryptKey;
		$signature = md5($paramsStr);
		$params['signature'] = $signature;
		
		return self::HOST.'api/constructor.html?'.http_build_query($params);
	}
	
	function langIframe($widget_code, $language)
	{		
		return str_replace('div data-', 'div data-lang="'.$language.'" data-', $widget_code);
	}
	
	function horizAlligmentIframe($widget_code, $allign)
	{
		return str_replace('div data-', 'div style="text-align:'.$allign.';" data-', $widget_code);
	}
}