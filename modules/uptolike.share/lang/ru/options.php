<?
$MESS["SHARE_TAB_CONSTRUCTOR"] = "Конструктор";
$MESS["SHARE_TAB_CONSTRUCTOR_TITLE"] = "Конструктор кнопок";

$MESS["SHARE_TAB_STATISTIC"] = "Статистика";
$MESS["SHARE_TAB_STATISTIC_TITLE"] = "Статистика";

$MESS["SHARE_TAB_SETTINGS"] = "Настройки";
$MESS["SHARE_TAB_SETTINGS_TITLE"] = "Настройки отображения блока Uptolike";

$MESS["SHARE_TAB_LANGUAGE"] = "Язык:";
$MESS["SHARE_LANGUAGE_RU"] = "Русский";
$MESS["SHARE_LANGUAGE_EN"] = "Английский";
$MESS["SHARE_LANGUAGE_UA"] = "Украинский";	
$MESS["SHARE_LANGUAGE_DE"] = "Немецкий";
$MESS["SHARE_LANGUAGE_ES"] = "Испанский";
$MESS["SHARE_LANGUAGE_IT"] = "Итальянский";
$MESS["SHARE_LANGUAGE_LT"] = "Латвийский";
$MESS["SHARE_LANGUAGE_PL"] = "Польский";

$MESS["SHARE_TAB_HORIZ_ALLIGMENT"] = "Выравнивание:";
$MESS["SHARE_HORIZ_ALLIGMENT_LEFT"] = "По левому краю";
$MESS["SHARE_LANGUAGE_CENTER"] = "По центру";
$MESS["SHARE_LANGUAGE_RIGHT"] = "По правому краю";

$MESS["SHARE_ERROR"] = "Ошибка";
$MESS["SHARE_TAB_MAIL"] = "Email:";
$MESS["SHARE_TAB_KEY"] = "Ключ:";
$MESS["SHARE_TAB_WIJET_CODE"] = "Код виджета:";
$MESS["SHARE_TAB_WIJET_SETTINGS"] = "Настройки виджета:";

$MESS["SHARE_TAB_MESS_1"] = "Введите ваш адрес электронной почты для получения ключа.";

$MESS["SHARE_TAB_MESS_3"] = "Введен неверный ключ! Убедитесь что вы скопировали ключ без лишних символов (пробелов и т.д.)";
$MESS["SHARE_TAB_MESS_4"] = "Данный проект принадлежит другому пользователю. Обратитесь в службу поддержки";
$MESS["SHARE_TAB_BTN_GET_KEY"] = "Получить ключ";
$MESS["SHARE_TAB_BTN_AUTORIZ"] = "Авторизация";

$MESS["SHARE_TAB_MAIL_ILLEGAL_ARGUMENTS"] = '<span style="color:red;">Email пустой или указан неверно.</span>';
$MESS["SHARE_TAB_MAIL_ALREADY_EXISTS"] = '<span style="color:red;">Пользователь с таким email уже зарегистрирован, обратитесь в службу поддержки: <a href="mailto:uptolikeshare@gmail.com">uptolikeshare@gmail.com</a></span>';
$MESS["SHARE_TAB_MAIL_SENDED"] = 'На ваш адрес электронной почты отправлен секретный ключ. Введите его в поле ниже<br>Если письмо с ключом долго не приходит, возможно оно попало в Спам.<br>Если ключ так и не был получен напишите письмо в службу поддержки: <a href="mailto:uptolikeshare@gmail.com">uptolikeshare@gmail.com</a>';

$MESS["SHARE_TAB_TEXT1"] = 'Для вывода кнопок используйте наш компонент <i><b>Uptolike виджет (uptolike:uptolike.share)</b></i>, который находится во кладке <b>комопненты</b> визуального редактора.';
$MESS["SHARE_TAB_TEXT2"] = 'Данный модуль полностью бесплатен. Мы регулярно его улучшаем и добавляем новые функции.<br>Пожалуйста, оставьте свой отзыв на <a href="http://marketplace.1c-bitrix.ru/solutions/uptolike.share/#tab-rating-link">данной странице</a>. Спасибо!';
$MESS["SHARE_TAB_TEXT3"] = '<a href="http://uptolike.ru">Uptolike.ru</a> - конструктор социальных кнопок для вашего сайта с расширенным функционалом.<br>Служба поддержки: <a href="mailto:uptolikeshare@gmail.com">uptolikeshare@gmail.com</a>';
$MESS["SHARE_TAB_TEXT4"] = 'Пожалуйста, оставьте свой отзыв на <a href="http://marketplace.1c-bitrix.ru/solutions/uptolike.share/#tab-rating-link">данной странице</a>. Спасибо!';