<?
if(!$USER->IsAdmin())
	return;
	
$module_id = "uptolike.share";
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/include.php");
IncludeModuleLangFile(__FILE__);

$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($MODULE_RIGHT >= "R"):
/*
if ($REQUEST_METHOD == "GET" && $MODULE_RIGHT >= "W" && strlen($RestoreDefaults > 0 && check_bitrix_sessid())
{
	COption::RemoveOption("uptolike.share");
	$z = CGroup::GetList($v1 = "id", $v2 = "asc", array("ACTIVE" => "Y", "ADMIN" => "N"));
	while($zr = $z->Fetch())
		$APPLICATION->DelGroupRight($module_id, array($zr["ID"]));
}
*/
$message = false;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/include.php");

//$message = new CAdminMessage(GetMessage('SHARE_ERROR'));

if ($message)
	echo $message->Show();
	
$aTabs = array(
	array(
		"DIV" => "edit0",
		"TAB" => GetMessage("SHARE_TAB_CONSTRUCTOR"),
		"ICON" => "support_settings",
		"TITLE" => GetMessage("SHARE_TAB_CONSTRUCTOR_TITLE")
	),
	array(
		"DIV" => "edit1",
		"TAB" => GetMessage("SHARE_TAB_STATISTIC"),
		"ICON" => "support_settings",
		"TITLE" => GetMessage("SHARE_TAB_STATISTIC_TITLE")
	),
	array(
		"DIV" => "edit2",
		"TAB" => GetMessage("SHARE_TAB_SETTINGS"),
		"ICON" => "support_settings",
		"TITLE" => GetMessage("SHARE_TAB_SETTINGS_TITLE")
	),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if($message == ""):
if($REQUEST_METHOD == "POST" && strlen($Update.$Apply.$RestoreDefaults) > 0 && check_bitrix_sessid())
{
	if(strlen($RestoreDefaults) > 0)
	{
		COption::RemoveOption("uptolike.share");
		COption::SetOptionString($module_id, "SHARE_MAIL", $SHARE_MAIL);
		COption::SetOptionString($module_id, "SHARE_KEY", $SHARE_KEY);
		COption::SetOptionString($module_id, "MAIL_STATUS", $MAIL_STATUS);
		COption::SetOptionString($module_id, "KEY_OK", $KEY_OK);		
	}
	else
	{
		foreach($arAllOptions as $arOption)
		{
			$name=$arOption[0];
			$val=$_REQUEST[$name];
			if($arOption[2][0] == "checkbox" && $val != "Y")
				$val = "N";
			COption::SetOptionString("uptolike.share", $name, $val, $arOption[1]);
		}
		COption::SetOptionString($module_id, "SHARE_LANGUAGE", $SHARE_LANGUAGE);
		COption::SetOptionString($module_id, "SHARE_MAIL", $SHARE_MAIL);
		COption::SetOptionString($module_id, "SHARE_KEY", $SHARE_KEY);
		COption::SetOptionString($module_id, "WIJET_CODE", $WIJET_CODE);
		COption::SetOptionString($module_id, "WIJET_SETTINGS", $WIJET_SETTINGS);
		COption::SetOptionString($module_id, "MAIL_STATUS", $MAIL_STATUS);
		COption::SetOptionString($module_id, "KEY_OK", $KEY_OK);
		COption::SetOptionString($module_id, "HORIZ_ALLIGMENT", $HORIZ_ALLIGMENT);
	}
	if(strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
		LocalRedirect($_REQUEST["back_url_settings"]);
	else
		LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());
}
endif;

$SHARE_LANGUAGE = COption::GetOptionString($module_id, "SHARE_LANGUAGE");
$SHARE_MAIL = COption::GetOptionString($module_id, "SHARE_MAIL");
$SHARE_KEY = COption::GetOptionString($module_id, "SHARE_KEY");
$WIJET_CODE = COption::GetOptionString($module_id, "WIJET_CODE");
$WIJET_SETTINGS = COption::GetOptionString($module_id, "WIJET_SETTINGS");
$MAIL_STATUS = COption::GetOptionString($module_id, "MAIL_STATUS");
$KEY_OK = COption::GetOptionString($module_id, "KEY_OK");
$HORIZ_ALLIGMENT = COption::GetOptionString($module_id, "HORIZ_ALLIGMENT");

$tabControl->Begin();
?>
<form id="uptolike_form" method="post" action="<? echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<? echo LANGUAGE_ID?>">
	<input type="hidden" id="can_apply" name="can_apply" value="">
    <input type="hidden" id="mail_status" name="MAIL_STATUS" value="<?=htmlspecialcharsbx($MAIL_STATUS)?>">
    <input type="hidden" id="key_ok" name="KEY_OK" value="<?=htmlspecialcharsbx($KEY_OK)?>">    
    
<? $tabControl->BeginNextTab();?>
	<?
	if($SHARE_MAIL and $SHARE_KEY)
	{
		$construct_url = CUptolikeShare::constructorIframe($SHARE_MAIL, $SHARE_KEY);
	}
	else
	{
		$construct_url = CUptolikeShare::constructorIframe();
	}
    if($construct_url):
    ?>
    <iframe id='cons_iframe' style='height: 900px; min-width: 980px; width: 100%;' data-src="<?=$construct_url?>"></iframe>
    <?
    endif;
    ?>	
<? $tabControl->BeginNextTab();?>	
	<?
	$stat_url = '';
	if($SHARE_MAIL and $SHARE_KEY):
    	$stat_url = CUptolikeShare::statIframe($SHARE_MAIL, $SHARE_KEY);
	endif;
	if($SHARE_MAIL and empty($SHARE_KEY)):
		$auth = CUptolikeShare::userReg($SHARE_MAIL);
	endif;
	?>
    <? //var_dump($SHARE_MAIL, $SHARE_KEY, $stat_url, $auth);?>
    <? if($stat_url):?>
		<iframe id="stat_iframe" style="width: 100%; min-width: 980px; height:900px;" src="<?=htmlspecialcharsbx($stat_url)?>" data-src="<?=htmlspecialcharsbx($stat_url)?>"></iframe>
    <? endif;?>            
    <tr id="before_key_req" <? if($KEY_OK):?>style="display:none;"<? endif;?>>
    	<td></td>
		<td>
            <?=GetMessage("SHARE_TAB_MESS_1")?>
        </td>
    </tr>
	<tr id="uptolike_email_field" <? if($KEY_OK):?>style="display:none;"<? endif;?>>
		<td><?=GetMessage("SHARE_TAB_MAIL")?></td>
		<td>
        <input id="uptolike_email" type="text" name="SHARE_MAIL" value="<?=htmlspecialcharsbx($SHARE_MAIL)?>">
        </td>
	</tr>
    <tr id="message_user_box_field" <? if($KEY_OK):?>style="display:none;"<? endif;?>>
    	<td></td>
        <td><div id="message_user_box"></div></td>
    </tr>
    <tr id="uptolike_cryptkey_field" style="display:none;">
		<td><?=GetMessage("SHARE_TAB_KEY")?></td>
		<td>
        <input id="uptolike_cryptkey" type="text" name="SHARE_KEY" value="<?=htmlspecialcharsbx($SHARE_KEY)?>" style="width: 520px;">
        </td>
	</tr>
    <tr id="get_key_btn_field" <? if($KEY_OK):?>style="display:none;"<? endif;?>>
    	<td></td>
        <td><button id="get_key" type="button"><?=GetMessage("SHARE_TAB_BTN_GET_KEY")?></button></td>
    </tr>
    <tr id="bad_key_field" style="display:none;">
    	<td></td>
    	<td><span style="color:red;"><?=GetMessage("SHARE_TAB_MESS_3")?></span></td>
    </tr>
    <tr id="foreignAccess_field" style="display:none;">
    	<td></td>
    	<td><span style="color:red;"><?=GetMessage("SHARE_TAB_MESS_4")?></span></td>
    </tr>
    <tr id="key_auth_field" style="display:none;">
    	<td></td>
        <td><button id="auth" type="button"><?=GetMessage("SHARE_TAB_BTN_AUTORIZ")?></button></td>
    </tr>    	
<? $tabControl->BeginNextTab();?>	
	<tr>
		<td><?=GetMessage("SHARE_TAB_LANGUAGE")?></td>
		<td>
		<?
        $language = array(
            "ru" => GetMessage("SHARE_LANGUAGE_RU"),
            "en" => GetMessage("SHARE_LANGUAGE_EN"),
            "ua" => GetMessage("SHARE_LANGUAGE_UA"),	
            "de" => GetMessage("SHARE_LANGUAGE_DE"),
            "es" => GetMessage("SHARE_LANGUAGE_ES"),
            "it" => GetMessage("SHARE_LANGUAGE_IT"),
            "pl" => GetMessage("SHARE_LANGUAGE_PL"),
            "lt" => GetMessage("SHARE_LANGUAGE_LT"),
        );
		$arrLanguage = Array("reference" => array_values($language), "reference_id" => array_keys($language));
		echo SelectBoxFromArray("SHARE_LANGUAGE", $arrLanguage, $SHARE_LANGUAGE, "");
		?></td>
	</tr>
    <tr>
		<td><?=GetMessage("SHARE_TAB_HORIZ_ALLIGMENT")?></td>
		<td>
		<?
        $hirizalligment = array(
            "left" => GetMessage("SHARE_HORIZ_ALLIGMENT_LEFT"),
            "center" => GetMessage("SHARE_LANGUAGE_CENTER"),
            "right" => GetMessage("SHARE_LANGUAGE_RIGHT"),
        );
		$arrHorizAliligment = Array("reference" => array_values($hirizalligment), "reference_id" => array_keys($hirizalligment));
		echo SelectBoxFromArray("HORIZ_ALLIGMENT", $arrHorizAliligment, $HORIZ_ALLIGMENT, "");
		?></td>
	</tr>
    <tr style="display:none;">
		<td><?=GetMessage("SHARE_TAB_WIJET_CODE")?></td>
		<td>
        <textarea id="widget_code" name="WIJET_CODE"><?=htmlspecialcharsbx($WIJET_CODE)?></textarea>
        </td>
	</tr>
    <tr style="display:none;">
		<td><?=GetMessage("SHARE_TAB_WIJET_SETTINGS")?></td>
		<td>
        <input id="uptolike_json" type="text" name="WIJET_SETTINGS" value='<?=htmlspecialcharsbx($WIJET_SETTINGS)?>' style="width: 520px;">
        </td>
	</tr>
    <tr>
    	<td colspan="2"><div class="adm-info-message"><?=GetMessage("SHARE_TAB_TEXT1");?></div></td>
    </tr>
    <tr>
    	<td colspan="2"><div class="adm-info-message"><?=GetMessage("SHARE_TAB_TEXT2");?></div></td>
    </tr>
    <tr>
    	<td colspan="2"><div class="adm-info-message"><?=GetMessage("SHARE_TAB_TEXT3");?></div></td>
    </tr>
    <?php /*?><tr>
    	<td colspan="2"><div class="adm-info-message"><?=GetMessage("SHARE_TAB_TEXT4");?></div></td>
    </tr><?php */?>        
<? $tabControl->Buttons();?>
	<input id="uptolike_form_update" type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
	<input id="uptolike_form_apply" type="submit" name="Apply" value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
	<? if(strlen($_REQUEST["back_url_settings"])>0):?>
		<input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?=htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
		<input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
	<? endif?>
	<input type="submit" name="RestoreDefaults" title="<?=GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?=GetMessage("MAIN_RESTORE_DEFAULTS")?>">
	<?=bitrix_sessid_post();?>
<? $tabControl->End();?>
</form>

<script>
$(function(){
	
	var cons_iframe = 'cons_iframe';
	var $cons_iframe = $('#' + cons_iframe);	
	var stat_iframe = 'stat_iframe';
	var $stat_iframe = $('#' + stat_iframe);
	
	if($('#key_ok').val() != 'Y'){
		$stat_iframe.hide();
	}

	var onmessage = function(e){
		
		//console.log('e: ', e, 'e.data: ', e.data, 'e.data.action: ', e.data.action);
		
		if (e.data !== null && typeof e.data === 'object'){
		
			if (e.data.url.indexOf('constructor.html', 0) != -1){
				if ('ready' == e.data.action){							
					json = $('#uptolike_json').val();
					if(json !== ''){
						initConstr(json);
					};					
				};
				
				if ('getCode' == e.data.action){		
					if (('json' in e.data) && ('code' in e.data)){ 
						$('#widget_code').val(e.data.code); // - êîä âèäæåòà äëÿ âñòàâêè íà ñòðàíèöû ñàéòà		
						$('#uptolike_json').val(e.data.json); // - íàñòðîéêè áëîêà
						save_form();
					};
				};
			};
			
			//$('#' + stat_iframe).hide();
        	if (e.data.url.indexOf('statistics.html', 0) != -1){
				switch (e.data.action) {
					case 'badCredentials':
						$('#' + stat_iframe).hide();
						if (($('#uptolike_email').val() != '')) {
							$('#bad_key_field').show();
							//console.log('badCredentials');
						}	
						break;
					case 'foreignAccess':
						$('#' + stat_iframe).hide();
						$('#foreignAccess_field').show();
						break;
					case 'ready':						
						if($('#key_ok').val() == 'Y'){
							$('#' + stat_iframe).show();
						} else {
							$('#key_ok').val('Y');
							save_form();
						};
						break;
					case 'resize':
						if($('#key_ok').val() == 'Y'){
						} else {
							$('#key_ok').val('Y');
							save_form();
						};						
						break;
					default:
						//console.log(e.data.action);
				}		
			};
			
			if ('resize' == e.data.action && (typeof e.data.size != 'undefined')){
				if (('size' in e.data) && ('url' in e.data)){
					var h = e.data.size;
					var url = e.data.url;
					if(h > 0){
						$('iframe[src="' + url + '"]').css('height', h + 'px');
					}
					//console.log('resize url: ' + url + ', h: ' + h);
				};				
			};		
		};
	};
		
	var hideError = function(){
		var error_box = $('#error_uptolike');
		error_box.html('');
		error_box.hide();
	};	
	
	if (typeof window.addEventListener != 'undefined'){
		window.addEventListener('message', onmessage, false);
	} else if (typeof window.attachEvent != 'undefined'){
		window.attachEvent('onmessage', onmessage);
	};
	
	var getCode = function(){
		var win = document.getElementById(cons_iframe).contentWindow;
		win.postMessage({action: 'getCode'}, "*");
		return true;
	};
	
	var initConstr = function(jsonStr){
		var win = document.getElementById(cons_iframe).contentWindow;
		if ('' !== jsonStr) {
			win.postMessage({action: 'initialize', json: jsonStr}, "*");
		};
	};
	
	var can_apply = function(){
		var can_apply = $('#can_apply').val();
		if(can_apply){
			return true;
		}
		return false;
	}
	
	var set_apply_on = function(){
		$('#can_apply').val("Y");
	}
	
	var save_form = function(){
		set_apply_on();
		$('#uptolike_form_update').click();
	}
	
	$(document).on('click', '#uptolike_form_update, #uptolike_form_apply', function(e){
		if(can_apply()) return true;
		getCode();		
		return false;
	});
	
	$(document).on('click', '#get_key', function(){
        var email = $('#uptolike_email').val();
        regMe(email);
		return false;
    });	
	
	var regMe = function(my_mail) {
		str = jQuery.param({
			email: my_mail,
			partner: 'cms',
			projectId: 'cms' + document.location.host.replace(new RegExp("^www.","gim"),"").replace(/\-/g, '').replace(/\./g, ''),
			url:document.location.host.replace( new RegExp("^www.","gim"),"")});
		dataURL = "https://uptolike.com/api/getCryptKeyWithUserReg.json";
		$.getJSON(dataURL + "?" + str + "&callback=?", {}, function(result){
			console.log(result);
			var jsonString = JSON.stringify(result);
			var result = JSON.parse(jsonString);
			set_mail_status(result.statusCode);
			initialise();
			if ('MAIL_SENDED' == result.statusCode){
				save_form();
			}
		});
	};
	
	var set_mail_status = function(status){
		$('#mail_status').val(status);
	}
	
	var get_mail_status = function(){
		return $('#mail_status').val();
	}
	
	var message_user_box = function(message){
		$('#message_user_box').html(message);
	}
	
	var emailEntered = function(){
		$('#before_key_req').hide();
		$('#get_key_btn_field').hide();
		if($('#key_ok').val() != 'Y'){
			$('#uptolike_cryptkey_field').show();
			$('#key_auth_field').show();			
		}
		if($('#uptolike_cryptkey').val()){
			$('#message_user_box_field').hide();
		}
	}
	
	var initialise = function(){
		var mail_status = get_mail_status();
		if ('ALREADY_EXISTS' == mail_status) {
			message_user_box('<?=GetMessage("SHARE_TAB_MAIL_ALREADY_EXISTS")?>');
		} else if ('MAIL_SENDED' == mail_status) {
			message_user_box('<?=GetMessage("SHARE_TAB_MAIL_SENDED")?>');	
			emailEntered();	
		} else if ('ILLEGAL_ARGUMENTS' == mail_status) {
			message_user_box('<?=GetMessage("SHARE_TAB_MAIL_ILLEGAL_ARGUMENTS")?>');
		} else {
			message_user_box('');
		}
	}
	
	initialise();
	
	$(document).on('blur keydown change', '#uptolike_email', function(){
		if($(this).val()) $('#message_user_box').html('');
	});
	
	$(document).on('click', '#auth', function(){
		save_form();
		return false;
	});
		
	$.getScript("https://uptolike.com/api/getsession.json").done(function(script, textStatus) {
		$('#' + cons_iframe).attr('src', $('#' + cons_iframe).attr('data-src'));
		$('#' + stat_iframe).attr('src', $('#' + stat_iframe).attr('data-src'));
	});	
	
	$(document).on('click', '.adm-detail-tab', function(){
		$('#uptolike_form iframe').trigger('resize');
	});

});
</script>
<? endif;?>